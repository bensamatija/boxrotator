﻿using UnityEngine;
using System.Collections;

public class KeyPresses : MonoBehaviour {

    public Player_Raycast s_Player_Raycast;
    private bool rotationManipulationIsEnabled;
    private int bufferKeyUp;
    private int bufferKeyDown;
    private int bufferKeyLeft;
    private int bufferKeyRight;

    // We are not using arrays for now:
    public int[] keyArray;// = new int[4];
    public int keyNumSet = 0;
    public int keyNumGet = 0;
    public int currentArrayValue;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            //print("bufferUP");
            bufferKeyUp = s_Player_Raycast.bufferKeyUp;//GET
            bufferKeyUp = bufferKeyUp + 1;//SET
            s_Player_Raycast.bufferKeyUp = bufferKeyUp;//SEND

            // Set array:
         //   keyArray[keyNumSet] = 2;
         //   keyNumSet++;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            //print("bufferDOWN");
            bufferKeyDown = s_Player_Raycast.bufferKeyDown;//GET
            bufferKeyDown = bufferKeyDown + 1;//SET
            s_Player_Raycast.bufferKeyDown = bufferKeyDown;//SEND

            // Set array:
         //   keyArray[keyNumSet] = 4;
         //   keyNumSet++;
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            //print("bufferLEFT");
            bufferKeyLeft = s_Player_Raycast.bufferKeyLeft;//GET
            bufferKeyLeft = bufferKeyLeft + 1;//SET
            s_Player_Raycast.bufferKeyLeft = bufferKeyLeft;//SEND
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            //print("bufferRIGHT");
            bufferKeyRight = s_Player_Raycast.bufferKeyRight;//GET
            bufferKeyRight = bufferKeyRight + 1;//SET
            s_Player_Raycast.bufferKeyRight = bufferKeyRight;//SEND
        }

        // Reset array position:
        if (keyNumSet >= 5)
        {
            keyNumSet = 0;
        }


    }

	void FixedUpdate () {
        /*
        // Check if allowed to rotate:
        rotationManipulationIsEnabled = s_Player_Raycast.rotationManipulationIsEnabled;

        
        if (s_Player_Raycast.rotationManipulationIsEnabled == true)
        {
            // Get Array value:
            currentArrayValue = keyArray[keyNumSet];

            if (currentArrayValue == 2)
            {
                print("MOVE UP");
                s_Player_Raycast.rotationManipulationIsEnabled = false;
                s_Player_Raycast.rotate_XP90 = true;
                s_Player_Raycast.Rotate_XP90();
            }
            if (currentArrayValue == 4)
            {
                print("MOVE DOWN");
                s_Player_Raycast.rotationManipulationIsEnabled = false;
                s_Player_Raycast.rotate_XN90 = true;
                s_Player_Raycast.Rotate_XN90();
            }



           
        }//(s_Player_Raycast.rotationManipulationIsEnabled
        */



        
        //if (s_Player_Raycast.rotationManipulationIsEnabled == true)
        //{
        if ((s_Player_Raycast.bufferKeyUp > 0) && (s_Player_Raycast.rotationManipulationIsEnabled == true))
        {
            s_Player_Raycast.rotationManipulationIsEnabled = false;
            s_Player_Raycast.rotate_XP90 = true;
            s_Player_Raycast.Rotate_XP90();
        }
        if ((s_Player_Raycast.bufferKeyDown > 0) && (s_Player_Raycast.rotationManipulationIsEnabled == true))
        {
            s_Player_Raycast.rotationManipulationIsEnabled = false;
            s_Player_Raycast.rotate_XN90 = true;
            s_Player_Raycast.Rotate_XN90();
        }
        if ((s_Player_Raycast.bufferKeyLeft > 0) && (s_Player_Raycast.rotationManipulationIsEnabled == true))
        {
            s_Player_Raycast.rotationManipulationIsEnabled = false;
            s_Player_Raycast.rotate_ZP90 = true;
            s_Player_Raycast.Rotate_ZP90();
        }
        if ((s_Player_Raycast.bufferKeyRight > 0) && (s_Player_Raycast.rotationManipulationIsEnabled == true))
        {
            s_Player_Raycast.rotationManipulationIsEnabled = false;
            s_Player_Raycast.rotate_ZN90 = true;
            s_Player_Raycast.Rotate_ZN90();
        }
        //}




        //-------------------//
        /*   if (rotationManipulationIsEnabled == true)
           {
               if (Input.GetKeyDown(KeyCode.UpArrow))
               {
                   s_Player_Raycast.rotationManipulationIsEnabled = false;
                   //print ("Rotate_XP90");
                   s_Player_Raycast.rotate_XP90 = true;
                   s_Player_Raycast.Rotate_XP90();
               }
               if (Input.GetKeyDown(KeyCode.DownArrow))
               {
                   s_Player_Raycast.rotationManipulationIsEnabled = false;
                   //print ("Rotate_XN90");
                   s_Player_Raycast.rotate_XN90 = true;
                   s_Player_Raycast.Rotate_XN90();
               }

               if (Input.GetKeyDown(KeyCode.LeftArrow))
               {
                   s_Player_Raycast.rotationManipulationIsEnabled = false;
                   //print ("Rotate_ZP90");
                   s_Player_Raycast.rotate_ZP90 = true;
                   s_Player_Raycast.Rotate_ZP90();
               }
               if (Input.GetKeyDown(KeyCode.RightArrow))
               {
                   s_Player_Raycast.bufferKeyRight = bufferKeyRight + 1;
                   rotationManipulationIsEnabled = false;
                   //print ("Rotate_ZN90");
                   s_Player_Raycast.rotate_ZN90 = true;
                   s_Player_Raycast.Rotate_ZN90();
               }

               if (bufferKeyRight >= 1)
               {
                   s_Player_Raycast.rotate_ZN90 = true;
                   s_Player_Raycast.Rotate_ZN90();
                   //bufferKeyRight = bufferKeyRight - 1;
               }

           }//rotation manipulation

       */

    }
}
