﻿/*
 * Copyright (c) 2015-2016 Beebyte Limited. All rights reserved.
 */
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace Beebyte.Obfuscator
{
	public class Postbuild
	{
		private static readonly string CSHARP_DLL = "Assembly-CSharp.dll";
		private static Options options = null;
		private static readonly string[] extraAssemblyDirectories = new string[]
		{
			/* 
			 * AssemblyResolutionException can sometimes be thrown when obfuscating.
			 * Although most of these have now been prevented, due to individual system configuartions it is sometimes
			 * necessary to add an assembly's directory here, so that it may be found.
			 */
			@"C:\Path\To\Assembly\Directory",
			@"C:\Program Files (x86)\Microsoft SDKs\NETCoreSDK\System.Runtime\4.0.20\lib\netcore50",
			@"C:\Program Files (x86)\Microsoft SDKs\NETCoreSDK\System.Collections\4.0.0\ref\netcore50",
			@"C:\Program Files (x86)\Microsoft SDKs\NETCoreSDK\System.Collections\4.0.10\lib\netcore50",
			@"C:\Program Files (x86)\Microsoft SDKs\NETCoreSDK\System.Threading\4.0.0\ref\netcore50",
			@"C:\Program Files (x86)\Microsoft SDKs\NETCoreSDK\System.Diagnostics.Debug\4.0.0\ref\netcore50",
			@"C:\Program Files (x86)\Microsoft SDKs\NETCoreSDK\System.Runtime.Extensions\4.0.0\ref\netcore50",
			@"C:\Program Files (x86)\Microsoft SDKs\NETCoreSDK\System.Runtime.Extensions\4.0.0\ref\dotnet"
		};

		private static bool obfuscatedAfterScene = false;
		private static bool noCSharpScripts = false;
		private static bool needsReverting = false;

		[InitializeOnLoad]
		public static class PostbuildStatic
		{
			/*
			 * Often Unity's EditorApplication.update delegate is reset. Because it's so important to restore
			 * renamed MonoBehaviour assets we assign here where it will be called after scripts are compiled.
			 */ 
			static PostbuildStatic()
			{
				EditorApplication.update += RestoreAssets;
			}
		}

		[PostProcessBuild(1)]
		private static void PostBuildHook(UnityEditor.BuildTarget buildTarget, string pathToBuildProject)
		{
			if (obfuscatedAfterScene == false)
			{
				if (noCSharpScripts) Debug.LogWarning("No obfuscation required because no C# scripts were found");
				else Debug.LogError("Failed to obfuscate");
			}
			else if (needsReverting) RestoreAssets();

			Clear();
		}

		private static void Clear()
		{
			obfuscatedAfterScene = false;
			noCSharpScripts = false;

			if (options != null && options.obfuscateMonoBehaviourClassNames == false) Obfuscator.Clear();
		}

		[PostProcessScene(1)]
		public static void Obfuscate()
		{
			if (!EditorApplication.isPlayingOrWillChangePlaymode && !obfuscatedAfterScene)
			{
				try
				{
					EditorApplication.LockReloadAssemblies();
					string cSharpLocation = FindDllLocation(CSHARP_DLL);

					if (cSharpLocation == null) noCSharpScripts = true;
					else
					{
						if (options == null) options = OptionsManager.LoadOptions();

						Obfuscator.FixHexBug(options);

						if (options.enabled)
						{
							Obfuscator.SetExtraAssemblyDirectories(extraAssemblyDirectories);
							Obfuscator.Obfuscate(cSharpLocation, options, EditorUserBuildSettings.activeBuildTarget);

							if (options.obfuscateMonoBehaviourClassNames)
							{
								/*
								 * RestoreAssets must be called via the update delegate because [PostProcessBuild] is not guaranteed to be called
								 */
								EditorApplication.update += RestoreAssets;
								needsReverting = true;
							}
						}
						obfuscatedAfterScene = true;
					}
				}
				finally
				{
					EditorApplication.UnlockReloadAssemblies();
				}
			}
		}

		private static string FindDllLocation(string suffix)
		{
			foreach (System.Reflection.Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
			{
				try
				{
					if (assembly.Location == null || assembly.Location.Equals(string.Empty))
					{
						DisplayFailedAssemblyParseWarning(assembly);
					}
					else if (assembly.Location.EndsWith(suffix))
					{
						return assembly.Location;
					}
				}
				catch (System.NotSupportedException)
				{
					DisplayFailedAssemblyParseWarning(assembly);
				}
			}
			return null;
		}

		private static void DisplayFailedAssemblyParseWarning(System.Reflection.Assembly assembly)
		{
			Debug.LogWarning("Could not parse dynamically created assembly (string.Empty location) " + assembly.FullName + ". If you extend classes from within this assembly that in turn extend from MonoBehaviour you will need to manually annotate these classes with [Skip]");
		}

		/**
		 * This method restores obfuscated MonoBehaviour cs files to their original names.
		 */
		private static void RestoreAssets()
		{
#if UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
#else
			if (BuildPipeline.isBuildingPlayer == false)
			{
#endif
				try
				{
					EditorApplication.LockReloadAssemblies();
					Obfuscator.RevertAssetObfuscation();
					needsReverting = false;
					EditorApplication.update -= RestoreAssets;
				}
				finally
				{
					EditorApplication.UnlockReloadAssemblies();
				}
#if UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5 || UNITY_4_0 || UNITY_4_1 || UNITY_4_2
#else
			}
#endif
			Obfuscator.Clear();
		}
	}
}
