﻿using UnityEngine;
using System.Collections;

public class Spawner1 : MonoBehaviour {
	
	public int currentProjectileType;
	public Rigidbody projectileType1;
	public Rigidbody projectileType2;
	public Rigidbody projectileType3;
	public Rigidbody projectileType4;
	public Rigidbody projectileType5;
	public Rigidbody projectileType6;
    private float speed = 20;


    public bool b_spawner1 = false;
	public bool b_spawner2 = false;
	public bool b_spawner3 = false;
	public bool b_spawner4 = false;
	public bool b_spawner5 = false;
	public bool b_spawner6 = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
        // currentProjectileType:



        if (b_spawner1 == true)
		{
			b_spawner1 = false;
			Debug.Log ("Shoot1");
            Rigidbody instantiatedProjectile = Instantiate(projectileType1, transform.position,transform.rotation)as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, 0,speed));
		}
		if (b_spawner2 == true)
		{
			b_spawner2 = false;
			Debug.Log ("Shoot2");
            Rigidbody instantiatedProjectile = Instantiate(projectileType2,transform.position,transform.rotation)as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, 0,speed));
		}
		if (b_spawner3 == true)
		{
			b_spawner3 = false;
			Debug.Log ("Shoot3");
            Rigidbody instantiatedProjectile = Instantiate(projectileType3,transform.position,transform.rotation)as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, 0,speed));
		}
		if (b_spawner4 == true)
		{
			b_spawner4 = false;
			Debug.Log ("Shoot4");
            Rigidbody instantiatedProjectile = Instantiate(projectileType4,transform.position,transform.rotation)as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, 0,speed));
		}
		if (b_spawner5 == true)
		{
			b_spawner5 = false;
			Debug.Log ("Shoot5");
            Rigidbody instantiatedProjectile = Instantiate(projectileType5,transform.position,transform.rotation)as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, 0,speed));
		}
		if (b_spawner6 == true)
		{
			b_spawner6 = false;
			Debug.Log ("Shoot6");
            Rigidbody instantiatedProjectile = Instantiate(projectileType6,transform.position,transform.rotation)as Rigidbody;
            instantiatedProjectile.velocity = transform.TransformDirection(new Vector3(0, 0,speed));
		}
	}
}
