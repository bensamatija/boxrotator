﻿using UnityEngine;
using System.Collections;

public class SpawnerBody : MonoBehaviour
{
    public Rigidbody projectileType;
    public float enemy_speed;

    public void EnemySpawn()
    {
        //print(projectileType.name);
        Rigidbody enemy_instantiated = Instantiate(projectileType, transform.position, transform.rotation) as Rigidbody;
        enemy_instantiated.velocity = transform.TransformDirection(new Vector3(0, 0, enemy_speed));
    }
}