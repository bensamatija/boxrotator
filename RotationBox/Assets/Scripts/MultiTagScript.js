﻿#pragma strict


/*
To call a function in another Script do:
// Assign object that you want to use multi tags as MultiTag tag.

    if(object.tag ==”MultiTag”)

    {

    if(object.transform.GetComponent(TagScript).Tag(“Tag”))

    {

    //do something….

    }

    }

*/

var Tags : String[];

function Tag (TagX : String)

{

	for(var i=0;i<Tags.Length;i++)

	{

		var TempBool : boolean = (TagX ==Tags[i]);

		if(TempBool)

		{

		return true;

		}

	}

return false;

}