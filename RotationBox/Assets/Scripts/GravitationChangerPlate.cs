﻿using UnityEngine;
using System.Collections;

public class GravitationChangerPlate : MonoBehaviour {
	
	public GameObject PlayerMainCameraOBJ;
	private int currentPositionPLAYER;

	public int baseRotationNumber;
	public int destinationRotationNumber;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {		//we touch the plate
		//print ("Floor Trigger");
	/*	if(this.gameObject.tag == "T2T3"){		//
			print ("Rotate_ZP90");

		//	PlayerMainCameraOBJ.GetComponent<Player_Raycast>().rotate_ZP90 = true;
		//	PlayerMainCameraOBJ.GetComponent<Player_Raycast>().Rotate_XP90();
		}//tag*/

		currentPositionPLAYER = PlayerMainCameraOBJ.GetComponent<Player_Raycast>().currentPosition;
		//print (currentPositionPLAYER);

		if (currentPositionPLAYER == baseRotationNumber) {
			print ("currentPositionPLAYER == baseRotationNumber");

			PlayerMainCameraOBJ.GetComponent<Player_Raycast>().GravitationChangerPlateENABLED = true;		//enable
			PlayerMainCameraOBJ.GetComponent<Player_Raycast>().destinationRotationNumberPLATE = destinationRotationNumber;

		}


	}//trigger

}
