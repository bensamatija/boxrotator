using UnityEngine;
using System.Collections;

[RequireComponent (typeof (LineRenderer))]

public class LaserEmitter : MonoBehaviour 
{

	public GameObject laserHitSpark;
	private Transform thisObjTransform;
	private LineRenderer lineRenderer;

	private Ray ray;
	private RaycastHit hit;

//	private Vector3 reflectionDirection;

	public int reflectionsNumber = 2;
	public int lineRenderPoints;

	public GameObject lastHitedReflectorObject;

	public Vector3 sparkRotation;
	
	void Awake () 
	{
		//get the attached Transform component
		thisObjTransform = this.GetComponent<Transform>();
		//get the attached LineRenderer component
		lineRenderer = this.GetComponent<LineRenderer>();
	}

	void Update () 
	{
		reflectionsNumber = Mathf.Clamp(reflectionsNumber,1,reflectionsNumber);	//clamp the number of reflections between 1 and int capacity

		ray = new Ray(thisObjTransform.position,thisObjTransform.forward);	//cast a new ray forward, from the current attached game object position

		Debug.DrawRay (thisObjTransform.position,thisObjTransform.forward * 1, Color.blue);

		lineRenderPoints = reflectionsNumber;
		lineRenderer.SetVertexCount(lineRenderPoints); 
		lineRenderer.SetPosition(0,thisObjTransform.position);	//Set the first point of the line at the current attached game object position
		
		for(int i=0;i<=reflectionsNumber;i++)
		{
			//If the ray hasn't reflected yet
			if(i==0)
			{

				if(Physics.Raycast(ray.origin,ray.direction, out hit, 100))	//cast the ray 100 units at the specified direction  
				{  
					//the reflection direction is the reflection of the current ray direction flipped at the hit normal
				//	reflectionDirection = Vector3.Reflect(ray.direction,hit.normal);
					//cast the reflected ray, using the hit point as the origin and the reflected direction as the direction
				//	ray = new Ray(hit.point,reflectionDirection);					

					Debug.DrawRay (hit.point, hit.normal*1, Color.grey);
				//	Debug.DrawRay (hit.point, reflectionDirection * 1, Color.blue);
		//			Debug.Log("RaycastReflection(r1) hit at: " + hit.transform.name);





					//hit.rigidbody.AddForceAtPosition(transform.forward * 1, hit.point);
					//Instantiate(laserHitSpark, hit.point, Quaternion.identity);
					//Instantiate(laserHitSpark, hit.point, transform.rotation);

					//=======================================
					// Instantiate sparks where Laser Hit
					//=======================================
					sparkRotation = hit.normal;
					GameObject clone = (GameObject)Instantiate (laserHitSpark, hit.point, Quaternion.Euler(sparkRotation));	//Quaternion.identity
					Destroy (clone, 0.5f);

					if(reflectionsNumber == 1)
					{
						//add a new vertex to the line renderer
						lineRenderer.SetVertexCount(++lineRenderPoints); 
					}

					lineRenderer.SetPosition(i+1,hit.point); //set the position of the next vertex at the line renderer to be the same as the hit point
					//--------------------------------------------------------------------------------------

					//====================================================
					//	R E F L E C T O R   H I T 
					//====================================================
					if(hit.transform.tag == "LaserReflector") {
		//				print ("reflector hit");
						//	Send to LaserCollector Script:
						hit.transform.gameObject.SendMessage("ReflectLaser");
						lastHitedReflectorObject = hit.transform.gameObject;
						
					}//tag
					//====================================================
					//	C O L L E C T O R   H I T 
					//====================================================
					if(hit.transform.tag == "LaserCollector") {
		//				print ("collector hit");
						//	Send to LaserCollector Script:
						hit.transform.gameObject.SendMessage("CollectorEnabled");
						lastHitedReflectorObject = hit.transform.gameObject;
						this.lineRenderer.enabled = true;
						
					}//tag
					//====================================================
					//	E L S E   H I T 
					//====================================================
					if ((hit.transform.tag != "LaserReflector") && (hit.transform.tag != "LaserCollector")) {
						print (this.transform.name +" sending OFF");
						//	Send to LaserCollector Script:
						//hit.transform.gameObject.SendMessage("ReflectLaserDisable");
						if (lastHitedReflectorObject.transform.tag != "LaserCollector") {
							lastHitedReflectorObject.gameObject.SendMessage("ReflectLaserDisable");
						}

								
					}//tag

				} 
			}//
			//else // the ray has reflected at least once, so the "if" code is skipped and only "else" part is cycling



			//===================================
			//	Needed only for Reflecting laser
			//===================================

	/*		else
			//if(i==1)
			{
				//set the number of points to be the same as the number of reflections
				//lineRenderPoints = reflectionsNumber - 1;

				//	Ray 2
				//================
				//Check if the ray has hit something
				if(Physics.Raycast(ray.origin,ray.direction, out hit, 100))//cast the ray 100 units at the specified direction  
				{
					//the refletion direction is the reflection of the ray's direction at the hit normal
					reflectionDirection = Vector3.Reflect(reflectionDirection,hit.normal);
					//cast the reflected ray, using the hit point as the origin and the reflected direction as the direction
					ray = new Ray(hit.point,reflectionDirection);
					
					//Draw the normal - only can be seen at the Scene tab for debugging purposes
					Debug.DrawRay (hit.point, hit.normal*1, Color.grey);
					//represent the ray using a line that can only be viewed at the scene tab
					Debug.DrawRay (hit.point, reflectionDirection * 1, Color.blue);
					
					//Print the name of the object the cast ray has hit, at the console
					Debug.Log("Hit object: " + hit.transform.name);


					//=======================
					// If we hit an Object
					if(hit.transform.tag == "LaserReflectionSurface") {
						//hit.rigidbody.AddForceAtPosition(transform.forward * 1, hit.point);
						//Instantiate(laserHitSpark, hit.point, Quaternion.identity);
						//Instantiate(laserHitSpark, hit.point, transform.rotation);
						GameObject clone = (GameObject)Instantiate (laserHitSpark, hit.point, Quaternion.identity);
						Destroy (clone, 0.5f);

						if (reflectionsNumber >= i) {
							print ("reflectionsNumber <i> change");
							lineRenderPoints = reflectionsNumber + 1;
							//add a new vertex to the line renderer
							lineRenderer.SetVertexCount(++lineRenderPoints); 
							//set the position of the next vertex at the line renderer to be the same as the hit point
							lineRenderer.SetPosition(i+1,hit.point);	
						}

						lineRenderer.SetPosition(i+1,hit.point); //set the position of the next vertex at the line renderer to be the same as the hit point
					}// collision


				}
			}//else
			*/
		}
	}
}
