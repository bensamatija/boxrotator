﻿using UnityEngine;
using System.Collections;

public class PlayerPickupObject : MonoBehaviour {

	// IMPORTANT
	// Assign PlayerPickupableObject script to any object you want to pick up. Give the object a Rigidbody.

	GameObject mainCamera;
	GameObject carriedObject;

	public float Distance = 3;		//player object Distance
	public float Smooth = 15;

	public bool Carrying;

	// Use this for initialization
	void Start () {
		mainCamera = GameObject.FindGameObjectWithTag ("MainCamera");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Carrying) {
			Carry(carriedObject);

			checkDropObject();

			rotateObject ();
		}
		else {
			Pickup ();
		}	
	}//update

	void Carry(GameObject o) {

		//o.transform.position = mainCamera.transform.position + mainCamera.transform.forward * Distance;
		o.transform.position = Vector3.Lerp (o.transform.position, mainCamera.transform.position + mainCamera.transform.forward * Distance, Time.deltaTime * Smooth);
	}

	void rotateObject () {
		// rotate to the right position
	}

	void Pickup () {
		if (Input.GetKeyDown (KeyCode.E)) {
			int x = Screen.width / 2;
			int y = Screen.height / 2 ;

			Ray ray = mainCamera.GetComponent<Camera>().ScreenPointToRay(new Vector3(x,y));
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)) {
				PlayerPickupableObject p = hit.collider.GetComponent<PlayerPickupableObject>();
				if (p != null) {
					Carrying = true;
					carriedObject = p.gameObject;
					p.gameObject.GetComponent<Rigidbody>().isKinematic = true;		//we can move it around and its not affected by gravity
				}
			}
		}
	}//pickup

	void checkDropObject() {
		if(Input.GetKeyDown (KeyCode.E)) {
			dropObject();
		}
	}
	
	void dropObject() {
		Carrying = false;
		carriedObject.gameObject.GetComponent<Rigidbody>().isKinematic = false;
		carriedObject = null;
	}
}

