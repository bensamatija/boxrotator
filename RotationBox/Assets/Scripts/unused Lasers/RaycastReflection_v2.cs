﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (LineRenderer))]

public class RaycastReflection_v2 : MonoBehaviour {

	public GameObject laserHitSpark;

	private Transform thisObjectTransform;
	private LineRenderer lineRenderer;

	private Ray ray;
	private RaycastHit hit;

	private Vector3 reflectionDirection;
	public int numberLaserReflections = 3;
	public int numberLaserPoints;

	private int i = 0;


	// Use this for initialization
	void Start () {

		thisObjectTransform = this.GetComponent<Transform> ();
		lineRenderer = this.GetComponent<LineRenderer> ();
	
	}
	
	// Update is called once per frame
	void Update () {

		numberLaserReflections = Mathf.Clamp (numberLaserReflections, 1, numberLaserReflections);

		ray = new Ray (thisObjectTransform.position, thisObjectTransform.forward);

		// Debug Ray:
		Debug.DrawRay (thisObjectTransform.position, thisObjectTransform.forward * 100, Color.blue);

		numberLaserReflections = numberLaserPoints;
		lineRenderer.SetVertexCount (numberLaserReflections);
		lineRenderer.SetPosition (0, thisObjectTransform.position);

	//	for (int i = 0; i <= numberLaserReflections; i++) {

			if (i == 0) {
				if(Physics.Raycast(ray.origin,ray.direction, out hit, 100)) {

					reflectionDirection = Vector3.Reflect(ray.direction,hit.normal);

					ray = new Ray(hit.point, reflectionDirection);

					Debug.DrawRay (hit.point, hit.normal * 1, Color.grey);
					Debug.DrawRay (hit.point, reflectionDirection * 100, Color.blue);
					Debug.Log("RaycastReflection(r1) hit at: " + hit.transform.name);

					if(hit.transform.tag == "TestCollider") {

						GameObject clone = (GameObject)Instantiate (laserHitSpark, hit.point, Quaternion.identity);
						Destroy (clone, 0.4f);

					/*	if(numberLaserReflections == 1)
						{
							//add a new vertex to the line renderer
							lineRenderer.SetVertexCount(++numberLaserPoints); 
						}*/

						if (numberLaserReflections >= i) {
							numberLaserPoints = numberLaserReflections + 1;
							lineRenderer.SetVertexCount (++numberLaserPoints);
							lineRenderer.SetPosition (i + 1, hit.point);

							i = 1;
						}
					}//collision
						
				}//if physics raycast
			}// if i == 0

		if (i == 1) {
			if(Physics.Raycast(ray.origin,ray.direction, out hit, 100)) {
				
				reflectionDirection = Vector3.Reflect(ray.direction,hit.normal);
				
				ray = new Ray(hit.point, reflectionDirection);
				
				Debug.DrawRay (hit.point, hit.normal * 1, Color.grey);
				Debug.DrawRay (hit.point, reflectionDirection * 100, Color.blue);
				Debug.Log("RaycastReflection(r1) hit at: " + hit.transform.name);
				
				if(hit.transform.tag == "TestCollider") {
					
					GameObject clone = (GameObject)Instantiate (laserHitSpark, hit.point, Quaternion.identity);
					Destroy (clone, 0.4f);
					
					/*	if(numberLaserReflections == 1)
						{
							//add a new vertex to the line renderer
							lineRenderer.SetVertexCount(++numberLaserPoints); 
						}*/
					
					if (numberLaserReflections >= i) {
						numberLaserPoints = numberLaserReflections + 1;
						lineRenderer.SetVertexCount (++numberLaserPoints);
						lineRenderer.SetPosition (i + 1, hit.point);
						
						i = 2;
					}
				}//collision
				
			}//if physics raycast
		}// if i == 1

		if (i == 2) {
			if(Physics.Raycast(ray.origin,ray.direction, out hit, 100)) {
				
				reflectionDirection = Vector3.Reflect(ray.direction,hit.normal);
				
				ray = new Ray(hit.point, reflectionDirection);
				
				Debug.DrawRay (hit.point, hit.normal * 1, Color.grey);
				Debug.DrawRay (hit.point, reflectionDirection * 100, Color.blue);
				Debug.Log("RaycastReflection(r1) hit at: " + hit.transform.name);
				
				if(hit.transform.tag == "TestCollider") {
					
					GameObject clone = (GameObject)Instantiate (laserHitSpark, hit.point, Quaternion.identity);
					Destroy (clone, 0.4f);
					
					/*	if(numberLaserReflections == 1)
						{
							//add a new vertex to the line renderer
							lineRenderer.SetVertexCount(++numberLaserPoints); 
						}*/
					
					if (numberLaserReflections >= i) {
						numberLaserPoints = numberLaserReflections + 1;
						lineRenderer.SetVertexCount (++numberLaserPoints);
						lineRenderer.SetPosition (i + 1, hit.point);
						
						i = 3;
					}
				}//collision
				
			}//if physics raycast
		}// if i == 2
	//	}//for

	}
}
