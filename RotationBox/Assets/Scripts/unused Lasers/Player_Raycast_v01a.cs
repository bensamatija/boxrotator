﻿using UnityEngine;
using System.Collections;

public class Player_Raycast_v01a : MonoBehaviour {

	public GameObject Player;						// For finding the Current Position
	public GameObject WholeLevel;					// For Rotating

	public bool rotating = false;
	public bool rotatingAllowed = true;

	public float rotationTime = 0.0f;

	private float rotationSpeed = 100.0f;
	public float targetAngle;

	public int YP90_angle = 0;

	public int currentNumber = 2;

	public bool rotate_YP90 = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {		// FixedUpdate
		//-------------------//
		//	KEYBOARD & MOUSE
		//-------------------//
		if (Input.GetMouseButtonDown(0)) {
			print ("LMB");
			//-------------------//
			//	 R A Y C A S T
			//-------------------//
			Ray ray = GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 1000)) {
				//==========================================================================
				//		R O T A T E   T O   n 1 
				//==========================================================================
				if ((hit.transform.tag == "n1")) {		// (hit.distance <= 1.3)
					print ("Rotate to n1");
					//==========================================================================
					//		C u r r e n t   P o s i t i o n
					//==========================================================================
					if (currentNumber == 2) {
						print ("current is n2");
						rotate_YP90 = true;



						//StartCoroutine(Rotate_YP90());

						
						//RotateObject(transform, Vector3.up * -90, 0.5f);

						//StartCoroutine(Rotate_XP90());

						//float angle = Mathf.MoveTowardsAngle(transform.eulerAngles.x, targetAngle, rotationSpeed * Time.deltaTime);
						//WholeLevel.transform.eulerAngles = new Vector3(0, angle, 0);
						//transform.eulerAngles = new Vector3(0, targetAngle, 0);
						
						//transform.Rotate(Vector3.right * Time.deltaTime);
						//transform.Rotate(Vector3.up * Time.deltaTime, Space.World);

						//targetAngle = 90;

						//targetAngle = Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetAngle, rotationSpeed * Time.deltaTime);
						//WholeLevel.gameObject.transform.eulerAngles = new Vector3(0, targetAngle, 0);
					}
		
				}//transformTag
				//==========================================================================
				if ((hit.transform.tag == "n2")) {		// (hit.distance <= 1.3)
					print ("n2");

				}//transformTag
				//==========================================================================
			}//Ray 100
		}//Mouse Input 0
		//==========================================================================
		//		C H O S E   C O R R E C T   R O T A T I O N
		//==========================================================================

		if (rotate_YP90 == true) {
			print ("1");
			Rotate_YP90 ();		
		}


		//targetAngle = Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetAngle, rotationSpeed * Time.deltaTime);
		
		//float angle = Mathf.MoveTowardsAngle(transform.eulerAngles.y, targetAngle, rotationSpeed * Time.deltaTime);
		//WholeLevel.gameObject.transform.eulerAngles = new Vector3(0, targetAngle, 0);

	}//update

	//==========================================================================
	//		I N I T I A T E   R O T A T I O N
	//==========================================================================
	void Rotate_YP90 () {
		print ("2");
		Vector3 rot = WholeLevel.transform.rotation.eulerAngles;
		rot.y = rot.y + rotationSpeed * Time.deltaTime;
		if (YP90_angle == 0) {		//if ((rot.y >= 90) && (rot.y < 95)) {
			print ("rotated to 90");
			rotationSpeed = 0.0f;										// Stop rotating
			rotate_YP90 = false;
			YP90_angle = 90;											// Fix the Angle
		}
		if (YP90_angle == 90) {
			print ("rotated to 180");
			rotationSpeed = 0.0f;										// Stop rotating
			rotate_YP90 = false;
			YP90_angle = 180;											// Fix the Angle
		}
		WholeLevel.transform.eulerAngles = rot;
	}



/*	void FixedUpdate () {
		if (rotating == true){
			rotationTime = rotationTime + Time.deltaTime;
			WholeLevel.gameObject.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed); 
			if (rotationTime >= 2.0f) {
				//rotationTime = 0.0f;
				rotating = false;

			}
		}
	}*/
	/*
	void RotateObject () {
		if (rotating) {
		rotating = true;
		}

		Quaternion startRotation = WholeLevel.transform.rotation;
		Quaternion endRotation = WholeLevel.transform.rotation * Quaternion.Euler(degrees);
		float t = 0.0f;
		float rate = 1.0f/seconds;
		
		while (t < 1.0f) {
			t += Time.deltaTime * rate;
			WholeLevel.transform.rotation = Quaternion.Slerp(startRotation, endRotation, Mathf.SmoothStep(0.0f, 1.0f, t));
			//yield;
		}
		
		rotating = false;
	}

	IEnumerator Rotate_XP90 () {
		WholeLevel.gameObject.transform.Rotate(Vector3.up * Time.deltaTime * +90);
		yield return new WaitForSeconds (1.1f);
	}*/
}//end
