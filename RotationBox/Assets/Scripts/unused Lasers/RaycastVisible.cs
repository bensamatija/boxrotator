﻿using UnityEngine;
using System.Collections;

public class RaycastVisible : MonoBehaviour {

	public LineRenderer line;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update() {
		Vector3 rayDirection = transform.TransformDirection(Vector3.forward);
		if (Physics.Raycast(transform.position, rayDirection, 1000)) {

			print("There is something in front of the object!");
			
			//Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 50, Color,red);
		}



		//when a key is pressed do something
		if(Input.GetKey("f"))
		{
			//enable the line
			line.GetComponent<Renderer>().enabled = true;
			Ray ray = Camera.main.ScreenPointToRay(transform.position);
			RaycastHit hit = new RaycastHit();
			if (Physics.Raycast(ray, out hit))
			{ 
				print ("hit");
			}
			//if nothing is being pressed then set the line back to false
			else
			{
				line.GetComponent<Renderer>().enabled = false;
			}
		}


	}
}
