﻿using UnityEngine;
using System.Collections;

public class LaserScript : MonoBehaviour
{
	LineRenderer line;

	public GameObject laserHitSpark;
	
	void Start ()
	{
		line = gameObject.GetComponent<LineRenderer>();
		//LineRenderer();
	//	line.enabled = false;
	}
	void Update ()
	{
		if(Input.GetKey("f"))
		{
			//print ("f");
			StopCoroutine("FireLaser");
			StartCoroutine("FireLaser");
		}
	}
	IEnumerator FireLaser()
	{
		line.enabled = true;
		
		while(Input.GetKey("f"))
		{
			// When the Laser is shown it offsets UV so it looks like its moving
			line.GetComponent<Renderer>().material.mainTextureOffset = new Vector2 (0, Time.time);

			Ray ray = new Ray(transform.position, transform.forward);
			RaycastHit hit;
			
			line.SetPosition(0, ray.origin);
			
			if(Physics.Raycast(ray, out hit, 100))
			{
				line.SetPosition(1, hit.point);

				//=======================
				// If we hit an Object
				if(hit.transform.tag == "")
				{
					//hit.rigidbody.AddForceAtPosition(transform.forward * 10, hit.point);
					//Instantiate(laserHitSpark, hit.point, Quaternion.identity);
					//Instantiate(laserHitSpark, hit.point, transform.rotation);
					GameObject clone = (GameObject)Instantiate (laserHitSpark, hit.point, Quaternion.identity);
					Destroy (clone, 0.5f);
				}
			}
			else
				line.SetPosition(1, ray.GetPoint(100));
			
			yield return null;
		}

		// When we stop the laser
		line.enabled = false;
		line.GetComponent<Renderer>().material.mainTextureOffset = new Vector2 (0, 0);
	}
}