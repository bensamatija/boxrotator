﻿using UnityEngine;
using System.Collections;

public class FadeInOut : MonoBehaviour {
	/*
	//------------------------------------------------------------
	//	Use This lines in another Script to Initiate the Fade
	//------------------------------------------------------------

	//public FadeInOut Switch:						// Move the same object to both places in Inspector. The GameObject must contain FadeInOut Script!
	public FadeInOut FadeInOut_Script;				// Assign Script as a new variable.
	public GameObject ScreenFader_Object;			// Assign object that contains the Script FadeInOut.

	// When you want to start Fade: (Note this must be initialized only once, not updatd every frame)
	FadeInOut_Script = ScreenFader_Object.GetComponent<FadeInOut> ();
	FadeInOut_Script.ScreenFadeIn_01 ();

	//------------------------------------------------------------
	//	Use This lines in another Script to Initiate the Fade
	//------------------------------------------------------------
	//#################################################################################################
	*/
	// Local:
	public bool fadeNow;

	public Texture FadeOutTexture;			// Overlay Image	(Black2x2p)
	private float fadeSpeed = 0.8f;			// Fade Speed

	private int drawDepth = -1000;			// The Texture's order in draw hierarchy: low numbers means it renders on top
	private float alpha = 1.0f;				// texture alpha value between 0 and 1
	private int fadeDirection = -1;			// direction to fade: in -1, out +1
	
	void Start () {	
	}
	void OnGUI () {
		if (fadeNow) {
			//print ("Doing the Fade now");
			// Fade in and out:
			alpha = alpha + fadeDirection * fadeSpeed * Time.deltaTime;
			// Force (clamp) the numbers between 0 and 1 because GUI.color uses alpha values between 0 and 1
			alpha = Mathf.Clamp01(alpha);
			// Set color of GUI:
			GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha);			// Set alpha value
			GUI.depth = drawDepth;															// black texture will render on top
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), FadeOutTexture);	// Fit entire screen
		}

		/*if (fadeNow == false) {
			alpha = 1;
		}*/
	}
	public float BeginFade (int direction) {
		fadeDirection = direction;
		return (fadeSpeed);					// Return the fadeSpeed variable so it's easy to time the Application.LoadLevel();
	}
	/*void OnLevelLoaded () {
		alpha = 1;							// Use this if alpha is not set to 1 by default
		BeginFade (-1);						// call the fade in function
	}*/

	//------------------------------------------------------------
	//			This is being called from other scripts:
	//------------------------------------------------------------
	public void ScreenFadeIn_01 () {
		fadeDirection = -1;					// direction to fade: in -1, out +1
		alpha = 1;							// texture alpha value between 0 and 1
		fadeSpeed = 0.05f;					// Fade Speed
		fadeNow = true;						// Do it now
		//print ("fadeNow = true");
	}
	public void ScreenFadeIn_02 () {
		fadeDirection = -1;					// direction to fade: in -1, out +1
		alpha = 1;							// texture alpha value between 0 and 1
		fadeSpeed = 0.4f;					// Fade Speed
		fadeNow = true;						// Do it now
		//print ("fadeNow = true");
	}
	//---------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------

	public void ScreenFadeOut_01 () {
		fadeDirection = -1;					// direction to fade: in -1, out +1
		alpha = 0;							// texture alpha value between 0 and 1
		fadeSpeed = -0.2f;					// Fade Speed
		fadeNow = true;						// Do it now
		//print ("fadeNow = true");
	}
	public void ScreenFadeOut_02 () {
		fadeDirection = -1;					// direction to fade: in -1, out +1
		alpha = 0;							// texture alpha value between 0 and 1
		fadeSpeed = -0.1f;					// Fade Speed
		fadeNow = true;						// Do it now
		//print ("fadeNow = true");
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}
}
