﻿using UnityEngine;
using System.Collections;

public class VolumetricLightBoxes : MonoBehaviour {

    public bool VolumetricLightBox = false;

    public GameObject Type1;
    public GameObject Type2;
    public GameObject Type3;
    public GameObject Type4;
    public GameObject Type5;
    public GameObject Type6;

    public Renderer rend1;
    public Renderer rend2;
    public Renderer rend3;
    public Renderer rend4;
    public Renderer rend5;
    public Renderer rend6;

    public bool b_Type1;
    public bool b_Type2;
    public bool b_Type3;
    public bool b_Type4;
    public bool b_Type5;
    public bool b_Type6;

    //public Renderer rend;
    public string rendX;

    //public float offsetX;
    private float offsetY = -0.54f;   //0.54
    public float time1;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (VolumetricLightBox)
        {
            // Enable Specific object:
            if (b_Type1) { Type1.gameObject.SetActive(true); }
            if (b_Type2) { Type2.gameObject.SetActive(true); }
            if (b_Type3) { Type3.gameObject.SetActive(true); }
            if (b_Type4) { Type4.gameObject.SetActive(true); }
            if (b_Type5) { Type5.gameObject.SetActive(true); }
            if (b_Type6) { Type6.gameObject.SetActive(true); }


            // Animation:
            time1 += Time.deltaTime;

            if(time1 >= 0.001f)
            {
                time1 = 0;
                offsetY = offsetY - 0.018f;  //0.03f
                rend1.material.mainTextureOffset = new Vector2(0, offsetY);
                rend2.material.mainTextureOffset = new Vector2(0, offsetY);
                rend3.material.mainTextureOffset = new Vector2(0, offsetY);
                rend4.material.mainTextureOffset = new Vector2(0, offsetY);
                rend5.material.mainTextureOffset = new Vector2(0, offsetY);
                rend6.material.mainTextureOffset = new Vector2(0, offsetY);

                // Stop Volumetric Light:
                if (offsetY <= -1.03f)//1.54f
                {
                    VolumetricLightBox = false;
                    offsetY = -0.54f;

                    // Disable Specific object:
                    if (b_Type1) { Type1.gameObject.SetActive(false); b_Type1 = false; }
                    if (b_Type2) { Type2.gameObject.SetActive(false); b_Type2 = false; }
                    if (b_Type3) { Type3.gameObject.SetActive(false); b_Type3 = false; }
                    if (b_Type4) { Type4.gameObject.SetActive(false); b_Type4 = false; }
                    if (b_Type5) { Type5.gameObject.SetActive(false); b_Type5 = false; }
                    if (b_Type6) { Type6.gameObject.SetActive(false); b_Type6 = false; }
                }
            }


        }

    }//update
}
