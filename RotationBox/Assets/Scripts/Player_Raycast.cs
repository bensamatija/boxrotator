﻿using UnityEngine;
using System.Collections;

public class Player_Raycast : MonoBehaviour {

	public GameObject PlayerObject;									// For finding the Current Position
	public GameObject WholeLevel;									// For Rotating

	public Transform RotationPivot;

    //public CharacterMotor CharacterMotor_Script;
    //public GameObject CharacterMotor_Object;
    // SPEED TIME:
    //300.00000000000000000000000000000f    0.145000025f
    //600.00000000000000000000000000000f    0.29000005f

    public float rotationSpeed;// = 0.0f;// = 300.00000000000000000000000000000f;               // speed: 300.0, 150.0f time: 0.5800001f
    private float rotationSpeedDefault;// = 0;
	public float rotationTime;
    public float rotationTimeLimit = 0;// = 0.290000050000f;	// Time to rotate with Speed. 0.29000005f
    //public Instantiate s_Instantiate;
    private float t_TimeOfRotation = 0;
    public bool b_ItIsRotating = false;

    private float currentPositionTimerDelay = 0.1f;
    public bool rotationManipulationIsEnabled = true;
    public int bufferKeyUp = 0;
    public int bufferKeyDown = 0;
    public int bufferKeyLeft = 0;
    public int bufferKeyRight = 0;
    public int bufferKeyA = 0;

    //- Current Positions	(Start always 2)
    public int currentPosition;

	//- Rotation enablers:
	public bool rotate_XP90 = false;
	public bool rotate_XN90 = false;
	public bool rotate_XP180 = false;
	public bool rotate_YP90 = false;
	public bool rotate_YN90 = false;
	public bool rotate_YP180 = false;
	public bool rotate_ZP90 = false;
	public bool rotate_ZN90 = false;
	public bool rotate_ZP180 = false;

    //- Current Rotation of the Current Number
    public bool checkAndCorrectCurrentRotation;

    public float Tx_rot;
	public bool Tx_rot0 = false;
	public bool Tx_rot90 = false;
	public bool Tx_rot180 = false;
	public bool Tx_rot270 = false;

	public float Ty_rot;
	public bool Ty_rot0 = false;
	public bool Ty_rot90 = false;
	public bool Ty_rot180 = false;
	public bool Ty_rot270 = false;

	public float Tz_rot;
	public bool Tz_rot0 = false;
	public bool Tz_rot90 = false;
	public bool Tz_rot180 = false;
	public bool Tz_rot270 = false;

	//- GravitationChangerPlate
	public bool GravitationChangerPlateENABLED = false;
	public int destinationRotationNumberPLATE;

	// T E S T I N G 

	public Vector3 myPosition;
	public Quaternion myRotation;

    public Vector3 dir;
	public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) {
		dir = point - pivot; // get point direction relative to pivot
		dir = Quaternion.Euler(angles) * dir; // rotate it
		point = dir + pivot; // calculate rotated point
		return point; // return it
	}

	// Use this for initialization
	void Start () {
		currentPosition = 2;
		//rotationSpeedDefault = rotationSpeed;
        bufferKeyRight = 0;

        //Get Speed and time from Instantiate script:
     //   rotationSpeed = s_Instantiate.rotationSpeed;
     //   rotationTimeLimit = s_Instantiate.rotationTimeLimit;
    }

	void FixedUpdate () {		// FixedUpdate

        if (Input.GetKeyDown(KeyCode.A))
        {
            rotationManipulationIsEnabled = false;
            rotate_ZP180 = true;
            //Rotate_ZP180();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            float x = WholeLevel.gameObject.transform.rotation.x;
            float y = WholeLevel.gameObject.transform.rotation.y;
            float z = WholeLevel.gameObject.transform.rotation.z;

            //WholeLevel.transform.localEulerAngles = new Vector3(y, y, z);

            print("consolidate");

            if (x >= -20 && x <= 20)
            { print("1"); WholeLevel.transform.localEulerAngles = new Vector3(0, y, z); }
            if (x >= 70 && x <= 110)
            { print("2"); WholeLevel.transform.localEulerAngles = new Vector3(90, y, z); }
            if (x >= 160 && x <= 200)
            { print("3"); WholeLevel.transform.localEulerAngles = new Vector3(180, y, z); }
        }

        //print(rotationTimeLimit);


        /*
        DOESNT WORK !!??
        if (b_ItIsRotating)
        {
            print("Checking");
            if (((WholeLevel.transform.rotation.eulerAngles.x == 0) ||
                (WholeLevel.transform.rotation.eulerAngles.x == 90) ||
                (WholeLevel.transform.rotation.eulerAngles.x == 180) ||
                (WholeLevel.transform.rotation.eulerAngles.x == 270) ||
                (WholeLevel.transform.rotation.eulerAngles.x == 360)) &&
                ((WholeLevel.transform.rotation.eulerAngles.y == 0) ||
                (WholeLevel.transform.rotation.eulerAngles.y == 90) ||
                (WholeLevel.transform.rotation.eulerAngles.y == 180) ||
                (WholeLevel.transform.rotation.eulerAngles.y == 270) ||
                (WholeLevel.transform.rotation.eulerAngles.y == 360)) &&
                ((WholeLevel.transform.rotation.eulerAngles.z == 0) ||
                (WholeLevel.transform.rotation.eulerAngles.z == 90) ||
                (WholeLevel.transform.rotation.eulerAngles.z == 180) ||
                (WholeLevel.transform.rotation.eulerAngles.z == 270) ||
                (WholeLevel.transform.rotation.eulerAngles.z == 360)))
            {
                print("STOP 1");
                print(t_TimeOfRotation);
                rotate_XP90 = false;
                rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                bufferKeyUp = bufferKeyUp - 1;
                b_ItIsRotating = false;
            }
        }

        */




        //rotationSpeed = rotationSpeedDefault;           // Set Speed
        /*
                //-------------------// Direct Manipulation
                //	KEYBOARD & MOUSE
                //-------------------//
                if (rotationManipulationIsEnabled == true)
                {
                    if (Input.GetKeyDown(KeyCode.UpArrow))
                    {
                        rotationManipulationIsEnabled = false;
                        //print ("Rotate_XP90");
                        rotate_XP90 = true;
                        Rotate_XP90();
                    }
                    if (Input.GetKeyDown(KeyCode.DownArrow))
                    {
                        rotationManipulationIsEnabled = false;
                        //print ("Rotate_XN90");
                        rotate_XN90 = true;
                        Rotate_XN90();
                    }

                    if (Input.GetKeyDown(KeyCode.LeftArrow))
                    {
                        rotationManipulationIsEnabled = false;
                        //print ("Rotate_ZP90");
                        rotate_ZP90 = true;
                        Rotate_ZP90();
                    }
                    if (Input.GetKeyDown(KeyCode.RightArrow))
                    {
                        bufferKeyRight = bufferKeyRight + 1;
                        rotationManipulationIsEnabled = false;
                        //print ("Rotate_ZN90");
                        rotate_ZN90 = true;
                        Rotate_ZN90();
                    }

                    if (bufferKeyRight >= 1)
                    {
                        rotate_ZN90 = true;
                        Rotate_ZN90();
                        //bufferKeyRight = bufferKeyRight - 1;
                    }


                }//rotation manipulation
                */

        //****************************************************************************
        //if (Input.GetMouseButtonDown(0)) {
        /*		if ((Input.GetMouseButtonDown(0)) || (GravitationChangerPlateENABLED == true)) {
                    ////print ("LMB");
                    //-------------------//
                    //	 R A Y C A S T
                    //-------------------//
                    Ray ray = GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
                    RaycastHit hit;

                    //if (Physics.Raycast(ray, out hit, 1000))
                    if ((Physics.Raycast(ray, out hit, 1000)) || (GravitationChangerPlateENABLED == true)) {
                        ////print ("ray hit");


                        // CURRENT POSITION IS VALID:
                        if ((currentPosition == 1) || (currentPosition == 2) || (currentPosition == 3) || (currentPosition == 4) || (currentPosition == 5) || (currentPosition == 6)) {
                            ////print ("a");

                        //	CharacterMotor_Script = CharacterMotor_Object.GetComponent<CharacterMotor> ();
                        //	CharacterMotor_Script.inputJump = true;

                        //	StartCoroutine (DelayGravity());
                        }


                        //=========================================================================================================
                        //		C U R R E N T   P O S I T I O N   1
                        //=========================================================================================================
                        if (currentPosition == 1) {
                            currentPosition = 100;

                            ////print ("current position is T1");
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   1   P L A T F O R M   R O T A T I O N   Y   0
                            //====================================================================================
                            if (Ty_rot0 == true) {
                                ////print (currentPosition +" | Ty_rot = " +Ty_rot);
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                    }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZP90 = true;	//yn
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_ZN90 = true;	//yp
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   1   P L A T F O R M   R O T A T I O N   Y   90
                            //====================================================================================
                            if (Ty_rot90 == true) {
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_XP90 = true;	//yn
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XN90 = true;	//yp
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   1   P L A T F O R M   R O T A T I O N   Y   180
                            //====================================================================================
                            if (Ty_rot180 == true) {
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZN90 = true;	//yn
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_ZP90 = true;	//yp
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   1   P L A T F O R M   R O T A T I O N   Y   270
                            //====================================================================================
                            if (Ty_rot270 == true) {
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 2) {
                                    rotate_XN90 = true;	//yn
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XP90 = true;	//yp
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                        }//current position 

                        //=========================================================================================================
                        //		C U R R E N T   P O S I T I O N   2
                        //=========================================================================================================
                        if (currentPosition == 2) {
                            currentPosition = 100;

                            ////print ("current position is T2");

                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   2   P L A T F O R M   R O T A T I O N   Y   0
                            //====================================================================================
                            if (Ty_rot0 == true) {
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   2   P L A T F O R M   R O T A T I O N   Y   90
                            //====================================================================================
                            if (Ty_rot90 == true) {
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   2   P L A T F O R M   R O T A T I O N   Y   180
                            //====================================================================================
                            if (Ty_rot180 == true) {
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   2   P L A T F O R M   R O T A T I O N   Y   270
                            //====================================================================================
                            if (Ty_rot270 == true) {
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                        }//current position 2

                        //=========================================================================================================
                        //		C U R R E N T   P O S I T I O N   3
                        //=========================================================================================================
                        if (currentPosition == 3) {
                            currentPosition = 100;

                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   3   P L A T F O R M   R O T A T I O N   Y   0
                            //====================================================================================
                            if (Ty_rot0 == true) {
                                ////print ("current position is T3");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_ZP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XP180 = true; 
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   3   P L A T F O R M   R O T A T I O N   Y   90
                            //====================================================================================
                            if (Ty_rot90 == true) {
                                ////print ("current position is T3");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_ZP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_XN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_ZN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   3   P L A T F O R M   R O T A T I O N   Y   180
                            //====================================================================================
                            if (Ty_rot180 == true) {
                                ////print ("current position is T3");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_ZN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   3   P L A T F O R M   R O T A T I O N   Y   270
                            //====================================================================================
                            if (Ty_rot270 == true) {
                                ////print ("current position is T3");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_ZN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_XP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_ZP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                        }//current position 3

                        //=========================================================================================================
                        //		C U R R E N T   P O S I T I O N   4
                        //=========================================================================================================
                        if (currentPosition == 4) {
                            currentPosition = 100;

                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   4   P L A T F O R M   R O T A T I O N   Y   0
                            //====================================================================================
                            if (Ty_rot0 == true) {
                                ////print ("current position is T4");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XP180 = true;	
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_XN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_ZN90 = true; 
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   4   P L A T F O R M   R O T A T I O N   Y   90
                            //====================================================================================
                            if (Ty_rot90 == true) {
                                ////print ("current position is T4");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XP180 = true;	
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_XP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_ZN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XN90 = true; 
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   4   P L A T F O R M   R O T A T I O N   Y   180
                            //====================================================================================
                            if (Ty_rot180 == true) {
                                ////print ("current position is T4");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XP180 = true;	
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_XP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZN90 = true;	//xp
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_ZP90 = true; 
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   4   P L A T F O R M   R O T A T I O N   Y   270
                            //====================================================================================
                            if (Ty_rot270 == true) {
                                ////print ("current position is T4");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XP180 = true;	
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_XN90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_ZP90 = true;	
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XP90 = true; 
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                        }//current position 4

                        //=========================================================================================================
                        //		C U R R E N T   P O S I T I O N   5
                        //=========================================================================================================
                        if (currentPosition == 5) {
                            currentPosition = 100;

                            ////print ("current position is T5");
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   5   P L A T F O R M   R O T A T I O N   Y   0
                            //====================================================================================
                            if (Ty_rot0 == true) {
                                    ////print ("current position is T5");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   5   P L A T F O R M   R O T A T I O N   Y   90
                            //====================================================================================
                            if (Ty_rot90 == true) {
                                ////print ("current position is T5");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   5   P L A T F O R M   R O T A T I O N   Y   180
                            //====================================================================================
                            if (Ty_rot180 == true) {
                                ////print ("current position is T5");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   5   P L A T F O R M   R O T A T I O N   Y   270
                            //====================================================================================
                            if (Ty_rot270 == true) {
                                ////print ("current position is T5");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T6") || destinationRotationNumberPLATE == 6) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_6());
                                }
                            }//T rotation
                        }//current position 5

                        //=========================================================================================================
                        //		C U R R E N T   P O S I T I O N   6
                        //=========================================================================================================
                        if (currentPosition == 6) {
                            currentPosition = 100;

                            ////print ("current position is T6");
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   6   P L A T F O R M   R O T A T I O N   Y   0
                            //====================================================================================
                            if (Ty_rot0 == true) {
                                ////print ("current position is T6");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   6   P L A T F O R M   R O T A T I O N   Y   90
                            //====================================================================================
                            if (Ty_rot90 == true) {
                                ////print ("current position is T6");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   6   P L A T F O R M   R O T A T I O N   Y   180
                            //====================================================================================
                            if (Ty_rot180 == true) {
                                ////print ("current position is T6");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                            }//T rotation
                            //====================================================================================
                            //		C U R R E N T   P O S I T I O N   6   P L A T F O R M   R O T A T I O N   Y   270
                            //====================================================================================
                            if (Ty_rot270 == true) {
                                ////print ("current position is T6");
                                //======================================================================
                                //		P L A T F O R M   T A G
                                //======================================================================
                                if ((hit.transform.tag == "T1") || destinationRotationNumberPLATE == 1) {
                                    rotate_ZN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_1());
                                }
                                if ((hit.transform.tag == "T2") || destinationRotationNumberPLATE == 2) {
                                    rotate_XN90 = true;
                                    StartCoroutine(changeCurrentPositionTo_2());
                                }
                                if ((hit.transform.tag == "T3") || destinationRotationNumberPLATE == 3) {
                                    rotate_ZP180 = true;
                                    StartCoroutine(changeCurrentPositionTo_3());
                                }
                                if ((hit.transform.tag == "T4") || destinationRotationNumberPLATE == 4) {
                                    rotate_ZP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_4());
                                }
                                if ((hit.transform.tag == "T5") || destinationRotationNumberPLATE == 5) {
                                    rotate_XP90 = true;
                                    StartCoroutine(changeCurrentPositionTo_5());
                                }
                            }//T rotation
                        }//current position 6

                    }//Ray 100
                }//Mouse Input 0
                */
        //==========================================================================
        //		C H O S E   C O R R E C T   R O T A T I O N   Y
        //========================================================================== 
        // Use this to directly manipulate rotation by each axis.
        // Here we only need 2 axis for this project.

        if (rotate_XP90 == true) {
			//print ("Rotate_XP90");
			Rotate_XP90 ();		
		}
		if (rotate_XN90 == true) {
			//print ("Rotate_XN90");
			Rotate_XN90 ();		
		}
		if (rotate_XP180 == true) {
			//print ("Rotate_XP180");
			Rotate_XP180 ();		
		}
		//==================================
		if (rotate_YP90 == true) {
			//print ("Rotate_YP90");
			Rotate_YP90 ();		
		}
		if (rotate_YN90 == true) {
			//print ("Rotate_YN90");
			Rotate_YN90 ();		
		}
		if (rotate_YP180 == true) {
			//print ("Rotate_YP180");
			Rotate_YP180 ();		
		}
		//==================================
		if (rotate_ZP90 == true) {
			//print ("Rotate_ZP90");
			Rotate_ZP90 ();		
		}
		if (rotate_ZN90 == true) {
			//print ("Rotate_ZN90");
			Rotate_ZN90 ();		
		}
		if (rotate_ZP180 == true) {
			//print ("Rotate_ZP180");
			Rotate_ZP180 ();		
		}


        if (checkAndCorrectCurrentRotation)
        {
            //================================================================================================================================
            // Check The orientation of the T2 Face when Its on the bottom, so we can work with correct rotations from there
            // Zaokrozi vrednosti kotov
            //==========================================================================
            //		C H E C K   C U R R E N T   R O T A T I O N   Y   T
            //==========================================================================
            Tx_rot = WholeLevel.gameObject.transform.rotation.eulerAngles.x;
            Ty_rot = WholeLevel.gameObject.transform.rotation.eulerAngles.y;
            Tz_rot = WholeLevel.gameObject.transform.rotation.eulerAngles.z;

            Vector3 _pos = WholeLevel.gameObject.transform.position;

            print("fixing rotation");
            if (Tx_rot >= -20 && Tx_rot <= 20)
            {
                print(WholeLevel.transform.rotation.eulerAngles.x);
                //WholeLevel.gameObject.transform.rotation = Quaternion.AngleAxis(30, Vector3.left);
                //Tx_rot = 0;
                Tx_rot0 = true;
                Tx_rot90 = false;
                Tx_rot180 = false;
                Tx_rot270 = false;             
                _pos.x = 0;
                //Quaternion rotation = Quaternion.LookRotation(_pos);
                //rotation *= Quaternion.Euler(0, 90, 0); // this add a 90 degrees Y rotation
             //   WholeLevel.gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
                //WholeLevel.gameObject.transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 1);
            }
            if (Tx_rot >= 70 && Tx_rot <= 100)
            {
                //Tx_rot = 90;
                Tx_rot0 = false;
                Tx_rot90 = true;
                Tx_rot180 = false;
                Tx_rot270 = false;
                _pos.x = 90;
            }
            if (Tx_rot >= 160 && Tx_rot <= 200)
            {
                //Tx_rot = 180;
                Tx_rot0 = false;
                Tx_rot90 = false;
                Tx_rot180 = true;
                Tx_rot270 = false;
                _pos.x = 180;
            }
            if (Tx_rot >= 250 && Tx_rot <= 290)
            {
                //Tx_rot = 270;
                Tx_rot0 = false;
                Tx_rot90 = false;
                Tx_rot180 = false;
                Tx_rot270 = true;
                _pos.x = 270;
            }
            //==========================================================================
            if (Ty_rot >= -20 && Ty_rot <= 20)
            {
                //Ty_rot = 0;
                Ty_rot0 = true;
                Ty_rot90 = false;
                Ty_rot180 = false;
                Ty_rot270 = false;
                _pos.y = 0;
            }
            if (Ty_rot >= 70 && Ty_rot <= 110)
            {
                //Ty_rot = 90;
                Ty_rot0 = false;
                Ty_rot90 = true;
                Ty_rot180 = false;
                Ty_rot270 = false;
                _pos.y = 90;
            }
            if (Ty_rot >= 160 && Ty_rot <= 200)
            {
                //Ty_rot = 180;
                Ty_rot0 = false;
                Ty_rot90 = false;
                Ty_rot180 = true;
                Ty_rot270 = false;
                _pos.y = 180;
            }
            if (Ty_rot >= 250 && Ty_rot <= 290)
            {
                //Ty_rot = 270;
                Ty_rot0 = false;
                Ty_rot90 = false;
                Ty_rot180 = false;
                Ty_rot270 = true;
                _pos.y = 270;
            }
            //==========================================================================
            if (Tz_rot >= -20 && Tz_rot <= 20)
            {
                //Tz_rot = 0;
                Tz_rot0 = true;
                Tz_rot90 = false;
                Tz_rot180 = false;
                Tz_rot270 = false;
                _pos.z = 0;
            }
            if (Tz_rot >= 70 && Tz_rot <= 110)
            {
                //Tz_rot = 90;
                Tz_rot0 = false;
                Tz_rot90 = true;
                Tz_rot180 = false;
                Tz_rot270 = false;
                _pos.z = 90;
            }
            if (Tz_rot >= 160 && Tz_rot <= 200)
            {
                //Tz_rot = 180;
                Tz_rot0 = false;
                Tz_rot90 = false;
                Tz_rot180 = true;
                Tz_rot270 = false;
                _pos.z = 180;
            }
            if (Tz_rot >= 250 && Tz_rot <= 290)
            {
                //Tz_rot = 270;
                Tz_rot0 = false;
                Tz_rot90 = false;
                Tz_rot180 = false;
                Tz_rot270 = true;
                _pos.z = 270;
            }
            //==========================================================================
        }
		// Revert to state before rotation:
		GravitationChangerPlateENABLED = false;




        if (WholeLevel.transform.rotation.eulerAngles.x >= 90)
        {

        }



        //========================================================================================================================
        //========================================================================================================================
        //========================================================================================================================
        //========================================================================================================================
        //		T E S T I N G   A R E A 
        //===================================

            // This was used for rotation around center of the level:
            //	WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.up, Time.deltaTime * rotationSpeed);


            // Find position of the Player:
            //	Vector3 CurrentPosition = PlayerObject.transform.position;			//position.x
            //	//print (CurrentPosition);
            //------------------------------
            // //prints out my current position and shows in Inspector:
            //	myPosition = transform.position;
            //	myPosition = RotationPivot.transform.position;
            //	myRotation = RotationPivot.transform.rotation;
            ////print (myPosition);
            ////print (myRotation);
            //	//print ("X: " +RotationPivot.transform.rotation.eulerAngles.x);					// eulerAngles.x
            //	//print ("Y: " +RotationPivot.transform.rotation.eulerAngles.y);
            //	//print ("Z: " +RotationPivot.transform.rotation.eulerAngles.z);
            //---------------------------------
            //	WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.up, Time.deltaTime * RotationP
            //----------------------------------

            //===============================================

            //===============================================







    }//FixedUpdate


    public void Rotate360x10()
    {
            //canvas.transform.eulerAngles =newVector3(90f, Camera.main.transform.eulerAngles.y                 ??
            rotationTime = rotationTime + Time.deltaTime;
			//WholeLevel.gameObject.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed); 
			WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.right, Time.deltaTime * rotationSpeed);
		//	RotationPivot.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed * (-1));
			RotationPivot.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed);
			if (rotationTime >= rotationTimeLimit) {		//4.980008f	Distance 750 stopinj 150 speed
				rotate_XP90 = false;
				rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                bufferKeyUp = bufferKeyUp - 1;
            }
    }


	//==========================================================================        // Used for Direct Manipulation
	//		I N I T I A T E   R O T A T I O N   Y
	//==========================================================================		//Space relativeTo = Space.World
	//----------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------
	//					X   P O S I T I V E   90
	//--------------------------------------------------------------
	public void Rotate_XP90 () {

        //b_ItIsRotating = true;

        if (rotate_XP90 == true){
            //print("rotate_XP90 == true");
            t_TimeOfRotation = t_TimeOfRotation + Time.deltaTime;
            //canvas.transform.eulerAngles =newVector3(90f, Camera.main.transform.eulerAngles.y                 ??
            rotationTime = rotationTime + Time.deltaTime;
			//WholeLevel.gameObject.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed); 
			WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.right, Time.deltaTime * rotationSpeed);
		//	RotationPivot.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed * (-1));
			RotationPivot.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed);
			if (rotationTime >= rotationTimeLimit) {		//4.980008f	Distance 750 stopinj 150 speed
				rotate_XP90 = false;
				rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                bufferKeyUp = bufferKeyUp - 1;
            }

            // TESTING AND MEASURING TIME NEEDED FOR ROTATION:
            // Set "rotationTimeLimit" to a little bigger value.
            // When the angle get over 90, you will get the time needed for rotation.
            if (WholeLevel.transform.rotation.eulerAngles.x >= 90.0)
            {
         //       print("Rotation Time = " + t_TimeOfRotation);
            }
            // TESTING OVER
        }
	}
	//--------------------------------------------------------------
	//					X   N E G A T I V E   90
	//--------------------------------------------------------------
	public void Rotate_XN90 () {
		if (rotate_XN90 == true){
			rotationTime = rotationTime + Time.deltaTime; 
			//WholeLevel.gameObject.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed);
			WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.right, Time.deltaTime * rotationSpeed * (-1));
		//	RotationPivot.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed);
			RotationPivot.transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed * (-1));
			if (rotationTime >= rotationTimeLimit) {		//4.980008f	Distance 750 stopinj 150 speed
				rotate_XN90 = false;
				rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                bufferKeyDown = bufferKeyDown - 1;
            }
		}
	}
    //--------------------------------------------------------------
    //					X   P O S I T I V E   180
    //--------------------------------------------------------------
    public void Rotate_XP180()
    {

        print("ROTATING FOR MEASURING:");
        print("Speed: " + rotationSpeed);
        t_TimeOfRotation = t_TimeOfRotation + Time.deltaTime;

        if (rotate_ZP180 == true)
        {
            rotationTime = rotationTime + Time.deltaTime;
            //WholeLevel.gameObject.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
            WholeLevel.gameObject.transform.RotateAround(RotationPivot.position, Vector3.forward, Time.deltaTime * rotationSpeed);
            //	RotationPivot.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed * (-1));
            RotationPivot.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
            if (rotationTime >= rotationTimeLimit * 10)
            {       //* 5) 4.980008f	Distance 750 stopinj 150 speed
                rotate_ZP180 = false;
                rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                //bufferKeyRight = bufferKeyRight - 1;
            }
        }

        // TESTING AND MEASURING TIME NEEDED FOR ROTATION:
        // Set "rotationTimeLimit" to a little bigger value.
        // When the angle get over 90, you will get the time needed for rotation.
        if (WholeLevel.transform.rotation.eulerAngles.x >= 90.0)
        {
            print("Rotation Time = " + t_TimeOfRotation);
        }
        // TESTING OVER

    }
    //----------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------
    //					Y   P O S I T I V E   90
    //--------------------------------------------------------------
    public void Rotate_YP90 () {
		if (rotate_YP90 == true){
			rotationTime = rotationTime + Time.deltaTime;
			//WholeLevel.gameObject.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed); 
			WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.up, Time.deltaTime * rotationSpeed);
		//	RotationPivot.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed * (-1));
			RotationPivot.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
			if (rotationTime >= rotationTimeLimit) {		//4.980008f	Distance 750 stopinj 150 speed
				rotate_YP90 = false;
				rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                //bufferKeyRight = bufferKeyRight - 1;
            }
		}
	}
	//--------------------------------------------------------------
	//					Y   N E G A T I V E   90
	//--------------------------------------------------------------
	public void Rotate_YN90 () {
		if (rotate_YN90 == true){
			rotationTime = rotationTime + Time.deltaTime;
			//WholeLevel.gameObject.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed * (-1));
			WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.up, Time.deltaTime * rotationSpeed * (-1));
		//	RotationPivot.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
			RotationPivot.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed * (-1));
			if (rotationTime >= rotationTimeLimit) {		//4.980008f	Distance 750 stopinj 150 speed
				rotate_YN90 = false;
				rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                //bufferKeyRight = bufferKeyRight - 1;
            }
		}
	}
	//--------------------------------------------------------------
	//					Y   P O S I T I V E   180
	//--------------------------------------------------------------
	public void Rotate_YP180 () {
		if (rotate_YP180 == true){
			rotationTime = rotationTime + Time.deltaTime;
			//WholeLevel.gameObject.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
			WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.up, Time.deltaTime * rotationSpeed);
		//	RotationPivot.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed * (-1));
			RotationPivot.transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);
			if (rotationTime >= rotationTimeLimit * 2) {		//4.980008f	Distance 750 stopinj 150 speed
				rotate_YP180 = false;
				rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                //bufferKeyRight = bufferKeyRight - 1;
            }
		}
	}
	//----------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------
	//					Z   P O S I T I V E   90
	//--------------------------------------------------------------
	public void Rotate_ZP90 () {
        t_TimeOfRotation = t_TimeOfRotation + Time.deltaTime;

        if (rotate_ZP90 == true){
			rotationTime = rotationTime + Time.deltaTime;         
			//WholeLevel.gameObject.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
			WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.forward, Time.deltaTime * rotationSpeed);
		//	RotationPivot.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed * (-1));
			RotationPivot.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
			if (rotationTime >= rotationTimeLimit) {		//4.980008f	Distance 750 stopinj 150 speed
				rotate_ZP90 = false;
				rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                bufferKeyLeft = bufferKeyLeft - 1;
            }
            // TESTING AND MEASURING TIME NEEDED FOR ROTATION:
            // Set "rotationTimeLimit" to a little bigger value.
            // When the angle get over 90, you will get the time needed for rotation.
            if (WholeLevel.transform.rotation.eulerAngles.z >= 180.0)
            {
     //           print("Rotation Time Z = " + t_TimeOfRotation);
            }
            // TESTING OVER
        }
    }
	//--------------------------------------------------------------
	//					Z   N E G A T I V E   90
	//--------------------------------------------------------------
	public void Rotate_ZN90 () {
        if (rotate_ZN90 == true){
			rotationTime = rotationTime + Time.deltaTime;
			//WholeLevel.gameObject.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed * (-1));
			WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.forward, Time.deltaTime * rotationSpeed * (-1));
		//	RotationPivot.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
			RotationPivot.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed * (-1));
			if (rotationTime >= rotationTimeLimit) {		//4.980008f	Distance 750 stopinj 150 speed
				rotate_ZN90 = false;
				rotationTime = 0.0f;
                rotationManipulationIsEnabled = true;
                bufferKeyRight = bufferKeyRight - 1;
            }
		}
	}
	//--------------------------------------------------------------
	//					Z   P O S I T I V E   180                       MOD !!
	//--------------------------------------------------------------
	public void Rotate_ZP180 () {

        print("ROTATING FOR MEASURING:");
        print("Speed: " + rotationSpeed);
        t_TimeOfRotation = t_TimeOfRotation + Time.deltaTime;

        if (rotate_ZP180 == true){
			rotationTime = rotationTime + Time.deltaTime;
			//WholeLevel.gameObject.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
			WholeLevel.gameObject.transform.RotateAround (RotationPivot.position, Vector3.forward, Time.deltaTime * rotationSpeed);
		//	RotationPivot.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed * (-1));
			RotationPivot.transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
			if (rotationTime >= rotationTimeLimit * 8) {		//* 5) 4.980008f	Distance 750 stopinj 150 speed
				rotate_ZP180 = false;
                rotationTime = 0.0f;
                print("Rotation Time = " + t_TimeOfRotation);
                rotationManipulationIsEnabled = true;
                //bufferKeyRight = bufferKeyRight - 1;
            }
		}

        // TESTING AND MEASURING TIME NEEDED FOR ROTATION:
        // Set "rotationTimeLimit" to a little bigger value.
        // When the angle get over 90, you will get the time needed for rotation.
        if (WholeLevel.transform.rotation.eulerAngles.z >= 359.999999f)
        {
           print("Rotation Time 2 = " + t_TimeOfRotation);
            rotate_ZP180 = false;
            rotationManipulationIsEnabled = true;
            print("STOP");
        }
        // TESTING OVER

    }
    //----------------------------------------------------------------------------------------------------------------------------
    //**************************************************************************************************
    IEnumerator changeCurrentPositionTo_1 () {
		yield return new WaitForSeconds (currentPositionTimerDelay);
		currentPosition = 1;
        rotationManipulationIsEnabled = true;
    }
	IEnumerator changeCurrentPositionTo_2 () {
		yield return new WaitForSeconds (currentPositionTimerDelay);
		currentPosition = 2;
        rotationManipulationIsEnabled = true;
    }
	IEnumerator changeCurrentPositionTo_3 () {
		yield return new WaitForSeconds (currentPositionTimerDelay);
		currentPosition = 3;
        rotationManipulationIsEnabled = true;
    }
	IEnumerator changeCurrentPositionTo_4 () {
		yield return new WaitForSeconds (currentPositionTimerDelay);
		currentPosition = 4;
        rotationManipulationIsEnabled = true;
    }
	IEnumerator changeCurrentPositionTo_5 () {
		yield return new WaitForSeconds (currentPositionTimerDelay);
		currentPosition = 5;
        rotationManipulationIsEnabled = true;
    }
	IEnumerator changeCurrentPositionTo_6 () {
		yield return new WaitForSeconds (currentPositionTimerDelay);
		currentPosition = 6;
        rotationManipulationIsEnabled = true;
    }
	//**************************************************************************************************



}//end
