﻿using UnityEngine;
using System.Collections;

public class CollisionOfBodyAndBox : MonoBehaviour
{
    public BaseCode _BaseCode;
    public string collidedObjetType;

    //***************************************************************************************************
    // This script is located on every box collider. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //***************************************************************************************************

    void OnCollisionEnter(Collision box_collided_with_enemy)
    {
        //  May happen several times per enemy collision, because of multiplecolliders.

        collidedObjetType = box_collided_with_enemy.gameObject.tag;

        if ((collidedObjetType == "BodyTypeX")
            || (collidedObjetType == "BodyType1")
            || (collidedObjetType == "BodyType2")
            || (collidedObjetType == "BodyType3")
            || (collidedObjetType == "BodyType4")
            || (collidedObjetType == "BodyType5")
            || (collidedObjetType == "BodyType6")
            //|| (collidedObjetType == "BodyType7N")
            )
        {
            print("Collided Enemy: " + collidedObjetType);
            //  Send info to the base:
            _BaseCode.EnemyCollisionWithBox(collidedObjetType);
            //  Destroy Collided Enemy:
            Destroy(box_collided_with_enemy.gameObject);
        }
    }

}
/*
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            // COLOR CHANGE
            // Get Material from the collided object:
            defaultMaterial = defaultObject.gameObject.GetComponentInChildren<Renderer>().materials[0];

            rend = BoxMesh.gameObject.GetComponentInChildren<Renderer>();
            //rend.enabled = false;
            rend.material = defaultMaterial;
            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            */
