﻿using UnityEngine;
using System.Collections;

public class SwipeTouch2 : MonoBehaviour {

    public Player_Raycast s_Player_Raycast;
    private int bufferKeyUp;
    private int bufferKeyDown;
    private int bufferKeyLeft;
    private int bufferKeyRight;

    public float coolDown = 0f;

    void LateUpdate()
    {
        coolDown = Mathf.Clamp(coolDown, 0f, 5f);
        coolDown += Time.deltaTime * 1f;
    }

    void OnTouchMoved()
    {
        if (coolDown >= 5f)
        {
            if (Input.GetTouch(1).deltaPosition.y >= 0.25f)
            { //swipe up
                print("UP");
                bufferKeyUp = s_Player_Raycast.bufferKeyUp;//GET
                bufferKeyUp = bufferKeyUp + 1;//SET
                s_Player_Raycast.bufferKeyUp = bufferKeyUp;//SEND
            }
            if (Input.GetTouch(1).deltaPosition.y <= -0.75f)
            { //swipe down
                print("DOWN");
                bufferKeyDown = s_Player_Raycast.bufferKeyDown;//GET
                bufferKeyDown = bufferKeyDown + 1;//SET
                s_Player_Raycast.bufferKeyDown = bufferKeyDown;//SEND

            }
        }
    }

    void FixedUpdate()
    {
        
    }
}