﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;

public class LogoAnimation_Play : MonoBehaviour
{
    public ChartBoost_ShowAds _ChartBoost_ShowAds;

    private bool fadeNow = false;

    //private float ligthIntensity = 0;
    private float t_fadeTime = 0;

    public GameObject Letter_M;
    public GameObject Letter_B;
    public GameObject Matsbenz7;
    public Light 
        light_1, 
        light_2, 
        light_3, 
        light_4, 
        light_5, 
        light_6;
    //public GameObject IntroNotice;
    public GameObject Sun;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(AnimationLogo());

    }

    // Update is called once per frame
    void Update()
    {
        if (fadeNow)
        {
            t_fadeTime += Time.deltaTime;
            if (t_fadeTime >= 0.001f)
            {
                t_fadeTime = 0;
                light_1.intensity -= 0.01f;
                light_2.intensity -= 0.01f;
                light_3.intensity -= 0.01f;
                light_4.intensity -= 0.01f;
                light_5.intensity -= 0.01f;
                light_6.intensity -= 0.01f;
            }
        }
    }

    IEnumerator AnimationLogo()
    {
        //_ChartBoost_ShowAds.CacheInterstitialAd();
        //Time.timeScale = 0;
        //print ("Play");
        Matsbenz7.gameObject.SetActive(false);

        Letter_M.gameObject.GetComponent<Animation>().Play("M|M_1");
        Letter_B.gameObject.GetComponent<Animation>().Play("B|B_1");

        //Camera1.gameObject.animation.Play ("C1|C_1");
        //Camera1.gameObject.animation.Play ("C2|C_2");
        //Logo.animation.Play ("

        yield return new WaitForSeconds(6.65f);
        //	Sun.gameObject.SetActive (false);

        Matsbenz7.gameObject.SetActive(true);
        //yield return new WaitForSeconds(2.0f);//3.33f

        fadeNow = true;

        yield return new WaitForSeconds(4.0f); //4.5f

        //Application.LoadLevel("Basic_Level_v0.7");
        //SceneManager.UnloadScene("Basic_Level_v0.7");
        SceneManager.LoadScene("Basic_Level_v0.7", LoadSceneMode.Single);
    }
}
