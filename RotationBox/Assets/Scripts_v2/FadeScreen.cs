﻿using UnityEngine;
using System.Collections;

public class FadeScreen : MonoBehaviour {

    // Screen goes Black

    public GameObject backgroundMesh;
    public Renderer rend;
    public Color myColor;
    public bool b_fadeEnabled = false;
    private float alpha = 0;
    private float t_fadeTime;

    // Use this for initialization
    void Start () {
        
	}

    // Update is called once per frame
    void Update()
    {
        // GO BLACK:
        if (b_fadeEnabled)
        {
            t_fadeTime += Time.deltaTime;
            if (t_fadeTime >= 0.01f)
            {
                t_fadeTime = 0;

                alpha += 0.01f;

                rend = backgroundMesh.gameObject.GetComponent<Renderer>();

                myColor.a = alpha;// = new Color(1, 1, 1, alpha);
                rend.material.SetColor("_Color", myColor);              

                if (alpha >= 1) //0.85f
                {
                    myColor.a = 0.85f;
                    alpha = 0;
                    b_fadeEnabled = false;
                }
            }
        }
    }
}
