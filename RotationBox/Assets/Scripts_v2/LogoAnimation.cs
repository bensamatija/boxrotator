﻿using UnityEngine;
using System.Collections;

public class LogoAnimation : MonoBehaviour {

	public GameObject Letter_M;
	public GameObject Letter_B;

	public GameObject Camera1;
	public GameObject Camera2;
	public GameObject Camera3;

	public GameObject Matsbenz7;
	public GameObject IntroNotice;
	public GameObject Sun;

	// Use this for initialization
	void Start () {
		StartCoroutine (AnimationLogo());
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator AnimationLogo () {
		//Time.timeScale = 0;
		//print ("Play");
		Matsbenz7.gameObject.SetActive (false);

		Letter_M.gameObject.GetComponent<Animation>().Play ("M|M_1");
		Letter_B.gameObject.GetComponent<Animation>().Play ("B|B_1");

		//Camera1.gameObject.animation.Play ("C1|C_1");
		//Camera1.gameObject.animation.Play ("C2|C_2");
		//Logo.animation.Play ("

		yield return new WaitForSeconds (6.65f);
	//	Sun.gameObject.SetActive (false);

		Matsbenz7.gameObject.SetActive (true);
		yield return new WaitForSeconds (3.33f);

		IntroNotice.gameObject.SetActive (true);
		yield return new WaitForSeconds (4.5f);

		//Application.LoadLevel ("MainMenu");
	}
}
