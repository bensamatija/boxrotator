﻿using System;
using System.Text;
using UnityEngine;
using ChartboostSDK;
using System.Collections.Generic;

public class ChartBoost_ShowAds : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CacheInterstitialAd()
    {
        print("Ads cached:");
        Chartboost.cacheInterstitial(CBLocation.Default);
    }

    public void ShowInterstitialAd()
    {
        print("Ads shown:");
        Chartboost.showInterstitial(CBLocation.Default);
    }
}
