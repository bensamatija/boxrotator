﻿using UnityEngine;
using System.Collections;

public class SwipeTouch : MonoBehaviour
{

    public float minSwipeDistY;
    public float minSwipeDistX;
    private Vector2 startPos;

    public Keyboard _Keyboard;
    private bool rotationManipulationIsEnabled;
    private int bufferKeyUp;
    private int bufferKeyDown;
    private int bufferKeyLeft;
    private int bufferKeyRight;

    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPos = touch.position;
                    break;

                case TouchPhase.Ended:
                    float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
                    if (swipeDistVertical > minSwipeDistY)
                    {
                        float swipeValue = Mathf.Sign(touch.position.y - startPos.y);

                        if (swipeValue > 0)
                        {
                            print("up");
                            _Keyboard.UP();
                        }

                        else if (swipeValue < 0)
                        {
                            print("down");
                            _Keyboard.DOWN();
                        }
                    }

                    float swipeDistHorizontal = (new Vector3(touch.position.x, 0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;
                    if (swipeDistHorizontal > minSwipeDistX)
                    {
                        float swipeValue = Mathf.Sign(touch.position.x - startPos.x);

                        if (swipeValue > 0)
                        {
                            print("right");
                            _Keyboard.RIGHT();
                        }
                        else if (swipeValue < 0)
                        {
                            print("left");
                            _Keyboard.LEFT();
                        }

                    }
                    break;
            }
        }
    }
}