﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class Score_UpDown : MonoBehaviour
{
    private bool
        volumetric_light;
    private int
        score_collected_enemy_value = 1
        ;
    public int
        score_current,
        score_highest
        ;
    public float
        volumetric_light_timer,
        volumetric_light_offsetY
        ;
    public string
        enemy_tag
        ;
    public Text
        score_gui,
        score_highest_gui
        ;
    public Renderer
        VolumetricLight_rendererType1,
        VolumetricLight_rendererType2,
        VolumetricLight_rendererType3,
        VolumetricLight_rendererType4,
        VolumetricLight_rendererType5,
        VolumetricLight_rendererType6
        ;
    public GameObject
        VolumetricLight_objectType1,
        VolumetricLight_objectType2,
        VolumetricLight_objectType3,
        VolumetricLight_objectType4,
        VolumetricLight_objectType5,
        VolumetricLight_objectType6
        ;

    /// <summary>
    /// This file is in SCORE_UPDOWN
    /// </summary>


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //  PLAY VOLUMETRIC LIGHT ANIMATION:
        if (volumetric_light)
        {
            //volumetric_light = false;
            //test();
            
            volumetric_light_timer += Time.deltaTime;

            if (volumetric_light_timer >= 0.001f)            
            {
                volumetric_light_timer = 0;
                VolumetricLight_EnableRenderer();
                // Stop Volumetric Light:
                if (volumetric_light_offsetY <= -1.03f)//1.54f
                { VolumetricLight_DisableSpecificType(); }
            }
        }
    }

    void OnCollisionEnter(Collision enemy_collected)
    {
        //  Identify by tag:
        enemy_tag = enemy_collected.gameObject.tag;
        //  Add Score points:
        ScoreADDpoint();
        //  Update GUI:
        ScoreUpdateGUI();
        //  Make effect for collecting enemy:       
        VolumetricLight_EnableSpecificType();
    }
    /*
    void test()
    {
        Debug.Log("test:");
        for (int i = 1; i <= 8; i++)
        {
            //Debug.Log(i);
            //volumetric_light_offsetY = i;
            VolumetricLight_EnableRenderer();
        }
        VolumetricLight_DisableSpecificType();
    }
    */
    void VolumetricLight_EnableRenderer()
    {
        volumetric_light_offsetY = volumetric_light_offsetY - 0.018f;  //0.03f
        if (enemy_tag == "BodyType1") { VolumetricLight_rendererType1.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_tag == "BodyType2") { VolumetricLight_rendererType2.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_tag == "BodyType3") { VolumetricLight_rendererType3.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_tag == "BodyType4") { VolumetricLight_rendererType4.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_tag == "BodyType5") { VolumetricLight_rendererType5.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_tag == "BodyType6") { VolumetricLight_rendererType6.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
    }
    void VolumetricLight_EnableSpecificType()
    {
        volumetric_light_offsetY = -0.54f;
        volumetric_light = true;
        if (enemy_tag == "BodyType1") { VolumetricLight_objectType1.gameObject.SetActive(true); }
        if (enemy_tag == "BodyType2") { VolumetricLight_objectType2.gameObject.SetActive(true); }
        if (enemy_tag == "BodyType3") { VolumetricLight_objectType3.gameObject.SetActive(true); }
        if (enemy_tag == "BodyType4") { VolumetricLight_objectType4.gameObject.SetActive(true); }
        if (enemy_tag == "BodyType5") { VolumetricLight_objectType5.gameObject.SetActive(true); }
        if (enemy_tag == "BodyType6") { VolumetricLight_objectType6.gameObject.SetActive(true); }
    }
    void VolumetricLight_DisableSpecificType()
    {
        volumetric_light = false;
        if (enemy_tag == "BodyType1") { VolumetricLight_objectType1.gameObject.SetActive(false); }
        if (enemy_tag == "BodyType2") { VolumetricLight_objectType2.gameObject.SetActive(false); }
        if (enemy_tag == "BodyType3") { VolumetricLight_objectType3.gameObject.SetActive(false); }
        if (enemy_tag == "BodyType4") { VolumetricLight_objectType4.gameObject.SetActive(false); }
        if (enemy_tag == "BodyType5") { VolumetricLight_objectType5.gameObject.SetActive(false); }
        if (enemy_tag == "BodyType6") { VolumetricLight_objectType6.gameObject.SetActive(false); }
    }
    void ScoreADDpoint()
    {
        //  Add score point:
        score_current += score_collected_enemy_value;
        //  Check if High score:
        score_highest = PlayerPrefs.GetInt("p1_score_highest");
        if (score_current > score_highest)
        {
            PlayerPrefs.SetInt("p1_score_highest", score_current);
        }
    }
    void ScoreUpdateGUI()
    {
        score_gui.text = "Score: " + (000000 + score_current).ToString();
    }
}