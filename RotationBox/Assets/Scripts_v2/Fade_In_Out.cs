﻿using UnityEngine;
using System.Collections;

public class Fade_In_Out : MonoBehaviour {

    public Renderer screenrender;
    public Color my_color;
    public bool fade_to_black = false;
    public bool fade_to_white = false;
    private float alpha = 0;
    private float t_fadeTime = 0;

    void Update()
    {
        if (fade_to_black)
        {
            t_fadeTime += Time.deltaTime;
            if (t_fadeTime >= 0.001f)
            {
                t_fadeTime = 0;
                // Increase Alpha:
                alpha += 0.03f;    //0.007f;

                my_color.a = alpha;// = new Color(1, 1, 1, alpha);
                screenrender.material.SetColor("_Color", my_color);

                if (alpha >= 1)
                {
                    alpha = 1;
                    fade_to_black = false;
                }
            }
        }
        if (fade_to_white)
        {
            t_fadeTime += Time.deltaTime;
            if (t_fadeTime >= 0.001f)
            {
                t_fadeTime = 0;
                // Decrease Alpha:
                alpha -= 0.01f;    //0.007f;

                my_color.a = alpha;// = new Color(1, 1, 1, alpha);
                screenrender.material.SetColor("_Color", my_color);

                if (alpha <= 0)
                {
                    alpha = 0;
                    fade_to_white = false;
                }
            }
        }
    }
}
