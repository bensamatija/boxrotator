﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Audio;

using System;
using System.Linq;

public class BaseCode : MonoBehaviour
{
    public
        float my_camera_zoom
        ;
    private bool
        game_is_on = false,
        main_menu_delay_countdown = false
        ;

    //  BOOLS:
    public bool
        GAME_start_from_beginning = true,
        GAME_end_the_game = false,
        immortal = false,
        GAME_paused = false
        ;

    //  DOUBLES:
    /*public double
        ;*/

    //  FLOATS:
    public float
        box_rotation_speed,
        box_rotation_time_limit,
        game_session_time = 0,
        my_camera_zoom_max,
        my_camera_zoom_increase_value,
        my_camera_zoom_timer,
        enemy_speed,
        enemy_speed_min,
        enemy_speed_current,
        enemy_speed_max,
        enemy_speed_increase_value,
        enemy_speed_increase_timer,
        enemy_spawn_position_check_timer,
        enemy_spawn_timer,
        enemy_spawn_time_density,
        enemy_spawn_time_timer,
        stress_level_value,
        stress_level_increase_decrease_value,
        main_menu_delay_timer,
        game_over_time,
        temporarySpeedIncreaseTime = 0
        ;

    //  INTS:
    public int
        S1z, S2z, S3x, S4x, S5y, S6y,
        //game_timer_difficutly_level,
        game_difficutly_level,
        //enemy_quantity_max,
        //enemy_quantity_min,
        //enemy_quantity_current,
        spawn_enemy_position,
        spawn_enemy_type,
        enemy_quantity_session_total,
        enemy_quantity_type_1,
        enemy_quantity_type_2,
        enemy_quantity_type_3,
        enemy_quantity_type_4,
        enemy_quantity_type_5,
        enemy_quantity_type_6,
        enemy_quantity_type_7
        ;

    //  OBJECTS:
    public
        Camera my_camera;
    private
        Rigidbody enemy_instantiated;
    public
        GameObject spawn_position_1, spawn_position_2, spawn_position_3, spawn_position_4, spawn_position_5, spawn_position_6,
        bonusDoubleTapCircle;
    public SpawnerBody
        s_S1B1, s_S1B2, s_S1B3, s_S1B4, s_S1B5, s_S1B6, //s_S1B7,
        s_S2B1, s_S2B2, s_S2B3, s_S2B4, s_S2B5, s_S2B6, //s_S2B7,
        s_S3B1, s_S3B2, s_S3B3, s_S3B4, s_S3B5, s_S3B6, //s_S3B7,
        s_S4B1, s_S4B2, s_S4B3, s_S4B4, s_S4B5, s_S4B6, //s_S4B7,
        s_S5B1, s_S5B2, s_S5B3, s_S5B4, s_S5B5, s_S5B6, //s_S5B7,
        s_S6B1, s_S6B2, s_S6B3, s_S6B4, s_S6B5, s_S6B6; //s_S6B7;
    //public SpawnerBody[] spawner_body = new SpawnerBody[36];

    // CODE REFERENCES:
    public BoxRot _BoxRot;
    public Fade_In_Out _Fade_In_Out;
    public Keyboard _Keyboard;
    public SwipeTouch3 _SwipeTouch;
    public BackgroundColorChanger _BackgroundColorChanger;
    public ChartBoost_ShowAds _ChartBoost_ShowAds;
    public ChartboostExample _ChartboostExample;

    // ---------- SCORE  ----------//
    private bool
        volumetric_light,
        gui_custom_text_enabled
        ;
    public bool
        bonusDoubleTapEnabled;
    private int
        score_collected_enemy_value = 1
        ;
    public int
        score_current,
        score_highest_n1, score_highest_n2, score_highest_n3,
        score_strike,
        score_strike_save1,
        score_strike_bonus_temp,
        score_strike_bonus_point
        ;
    public float
        volumetric_light_timer,
        volumetric_light_offsetY,
        gui_custom_text_timer,
        score_highest_t1, score_highest_t2, score_highest_t3
        ;
    private float
        doubleTapTimer
        ;
    public string
        enemy_collected_tag
        ;
    public Text
        gui_custom_text,
        score_gui,
        score_strike_bonus,
        //score_highest_gui,
        gui_score_highest_n1, gui_score_highest_n2, gui_score_highest_n3,
        gui_score_highest_t1, gui_score_highest_t2, gui_score_highest_t3,
        time_minutes_seconds_gui
        ;
    public Image
        stress_level_bar
        ;
    public AudioClip[] audioClip
        ;
    public Renderer
        VolumetricLight_rendererType1,
        VolumetricLight_rendererType2,
        VolumetricLight_rendererType3,
        VolumetricLight_rendererType4,
        VolumetricLight_rendererType5,
        VolumetricLight_rendererType6
        ;
    public GameObject
        VolumetricLight_objectType1,
        VolumetricLight_objectType2,
        VolumetricLight_objectType3,
        VolumetricLight_objectType4,
        VolumetricLight_objectType5,
        VolumetricLight_objectType6,
        S1, S2, S3, S4, S5, S6,
        ScreenMainMenu_gui,
        ScreenGameOn,
        HomeButton_gui,
        PauseButton_gui,
        GameRulesPicture
        ;



    void Start()
    {
        DefaultStartValues();
    }
    void DefaultStartValues()
    {
        //................................
        DestroyAllClonedEnemies();
        game_session_time = 0;
        score_current = 0;
        score_strike_bonus_temp = 0;
        score_strike_bonus_point = 0;
        //game_timer_difficutly_level = 1;
        game_difficutly_level = 1;
        ScoreUpdateGUI();
        ScreenMainMenu_gui.SetActive(false);
        HomeButton_gui.gameObject.SetActive(false);
        PauseButton_gui.gameObject.SetActive(true);
        GameRulesPicture.gameObject.SetActive(false);
        ScreenGameOn.gameObject.SetActive(true);
        gui_custom_text.text = "";
        //................................

        my_camera_zoom = 38;
        my_camera.orthographicSize = my_camera_zoom;

        enemy_spawn_position_check_timer = 99;
        S1z = -62;
        S2z = +62;
        S3x = -62;
        S4x = +62;
        S5y = -62;
        S6y = +62;
        spawn_position_1.transform.position = new Vector3(0, 0, S1z);
        spawn_position_2.transform.position = new Vector3(0, 0, S2z);
        spawn_position_3.transform.position = new Vector3(S3x, 0, 0);
        spawn_position_4.transform.position = new Vector3(S4x, 0, 0);
        spawn_position_5.transform.position = new Vector3(0, S5y, 0);
        spawn_position_6.transform.position = new Vector3(0, S6y, 0);

        enemy_spawn_time_density = 3.5f;
        enemy_speed = 1;
        enemy_speed_current = 1;

        stress_level_bar.fillAmount = 0;
        stress_level_value = 0;

        //stress_level_increase_decrease_value = 0.34f;

        UnityEngine.Random.seed = (int)System.DateTime.Now.Ticks;
        print(UnityEngine.Random.seed);
    }
    void Awake()
    {
        //  GRAPHICS SETTINGS (may not apply):  
        QualitySettings.vSyncCount = 0;  // 0 1 2 // VSync must be disabled
        Application.targetFrameRate = 60;
        //QualitySettings.antiAliasing = 1;

        //Resources.UnloadUnusedAssets();   // Use this to unload unused materials

        //  Reset score_highest:
        //PlayerPrefs.SetInt("p1_score_highest_n1", 0);
        //PlayerPrefs.SetInt("p1_score_highest_n2", 0);
        //PlayerPrefs.SetInt("p1_score_highest_n3", 0);
        //PlayerPrefs.SetString("p1_score_highest_t1", "0000.000");
        //PlayerPrefs.SetString("p1_score_highest_t2", "0000.000");
        //PlayerPrefs.SetString("p1_score_highest_t3", "0000.000");
    }
    void Update()
    {
        // RESTART GAME:
        if (GAME_start_from_beginning)
        {
            GAME_start_from_beginning = false;
            BeginningOpening();
            game_is_on = true;
            ScreenMainMenu_gui.SetActive(false);
            PauseButton_gui.gameObject.SetActive(true);
            _BackgroundColorChanger.Randomize();
            _Keyboard.input_enabled = true;
            _SwipeTouch.input_enabled = true;
            GameRulesPicture.gameObject.SetActive(false);
            //_ChartBoost_ShowAds.CacheInterstitialAd();
            //_ChartboostExample.CacheInterstitial();
        }
        if (GAME_end_the_game)
        {
            GAME_end_the_game = false;
            game_is_on = false;
            GameOverNow();
        }
        //  Dont Modify this Directly. Use "GAME_start_from_beginning" and "GAME_end_the_game".
        if (game_is_on)
        {
            GamePlay();
        }
        if (!game_is_on)
        {
            MainMenu();
        }
    }
    //----------------------------------------
    public void RestartGame(bool _GAME_start_from_beginning)    // This is called from main menu touch button. RestartGame(true)
    //----------------------------------------
    {
        GAME_start_from_beginning = _GAME_start_from_beginning;
    }
    //----------------------------------------
    public void Immortal()  // This is called from ingame button.
    //----------------------------------------
    {
        //print("immortal: " + immortal);
        if (immortal) { immortal = false; return; }
        if (!immortal) { immortal = true; return; }
    }
    void BeginningOpening()
    {
        DefaultStartValues();
        _Fade_In_Out.fade_to_white = true;
    }
    public void ReturnToHomeScreen()
    {
        GAME_end_the_game = true;
        GameRulesPicture.gameObject.SetActive(false);
        ScreenGameOn.gameObject.SetActive(true);
        PauseGame();
    }
    public void GameRulesScreen()
    {
        if (GameRulesPicture.gameObject.activeSelf == false)
        {
            GameRulesPicture.gameObject.SetActive(true);
            ScreenMainMenu_gui.gameObject.SetActive(false);
            PauseGame();
            ScreenGameOn.gameObject.SetActive(false);

        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------//
    //---------------------------------------------------- Do this while we Play game: ---------------------------------------------------//
    //------------------------------------------------------------------------------------------------------------------------------------//
    //----------------------------------------
    void GamePlay()
    //----------------------------------------
    {
        DifficultySetByTime();
        GameSessionTime();
        GameDifficulty();
        EnemyNextSpawnTime();
        //BoxRotationSpeed();
        // TemporarySpeedIncrease();
        EnemySpeed();
        EnemySpeedIncrease();
        CameraZooming();
        EnemySpawnPositionCheck();
        //DeleteObject();
        //CollectObjectType();
        BonusDoubleTap();
        //Score();
        ScoreVolumetricLight();
        GUIupdate();
        //ApplyGameSettings();
        GameOverCheck();
        //AdditionalOptions();
    }
    //------------------------------------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------------------------------------------//
    void MainMenu()
    {   // When the game_is_on = false:
        MainMenuDelay();

        // High Scores:
        gui_score_highest_n1.text = (PlayerPrefs.GetInt("p1_score_highest_n1") + " Pts");
        gui_score_highest_n2.text = (PlayerPrefs.GetInt("p1_score_highest_n2") + " Pts");
        gui_score_highest_n3.text = (PlayerPrefs.GetInt("p1_score_highest_n3") + " Pts");
        //gui_score_highest_t1.text = (PlayerPrefs.GetString("p1_score_highest_t1") + " s").ToString();
        //gui_score_highest_t2.text = (PlayerPrefs.GetString("p1_score_highest_t2") + " s").ToString();
        //gui_score_highest_t3.text = (PlayerPrefs.GetString("p1_score_highest_t3") + " s").ToString();
    }
    //----------------------------------------
    void GameSessionTime()
    //----------------------------------------
    {
        //Time.timeScale = 1;
        game_session_time += Time.deltaTime;
    }
    //----------------------------------------
    public void PauseGame()
    //----------------------------------------
    {
        if (GAME_paused)
        {
            GAME_paused = false; Time.timeScale = 1f; HomeButton_gui.gameObject.SetActive(false);
            _SwipeTouch.tapCounter = 0; return;
        }
        if (!GAME_paused)
        {
            GAME_paused = true; Time.timeScale = 0f; HomeButton_gui.gameObject.SetActive(true);
            _SwipeTouch.tapCounter = 0; return;
        }
    }
    //----------------------------------------
    void GameDifficulty()
    //----------------------------------------
    {
        //  Difficulty is managed with SCORE:
        if ((game_difficutly_level == 1) && (score_current >= 10)) { game_difficutly_level = 2; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 2) && (score_current >= 20)) { game_difficutly_level = 3; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 3) && (score_current >= 30)) { game_difficutly_level = 4; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 4) && (score_current >= 40)) { game_difficutly_level = 5; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 5) && (score_current >= 50)) { game_difficutly_level = 6; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 6) && (score_current >= 60)) { game_difficutly_level = 7; print("Level: " + game_difficutly_level); }
        /*
        //  Difficulty is managed with SCORE STRIKE:
        if ((game_difficutly_level == 1) && (score_strike - score_strike_save1 >= 05)) { score_strike_save1 = score_strike; game_difficutly_level = 2; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 2) && (score_strike - score_strike_save1 >= 05)) { score_strike_save1 = score_strike; game_difficutly_level = 3; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 3) && (score_strike - score_strike_save1 >= 05)) { score_strike_save1 = score_strike; game_difficutly_level = 4; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 4) && (score_strike - score_strike_save1 >= 05)) { score_strike_save1 = score_strike; game_difficutly_level = 5; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 5) && (score_strike - score_strike_save1 >= 05)) { score_strike_save1 = score_strike; game_difficutly_level = 6; print("Level: " + game_difficutly_level); }
        if ((game_difficutly_level == 6) && (score_strike - score_strike_save1 >= 05)) { score_strike_save1 = score_strike; game_difficutly_level = 7; print("Level: " + game_difficutly_level); }
        */
        /*
        //  Difficulty is managed with SESSION TIME:
        if ((game_session_time > 00) && (game_session_time < 45)) { game_timer_difficutly_level = 1; }
        if ((game_session_time > 45) && (game_session_time < 90)) { game_timer_difficutly_level = 2; }
        if ((game_session_time > 90) && (game_session_time < 135)) { game_timer_difficutly_level = 3; }
        if ((game_session_time > 135) && (game_session_time < 180)) { game_timer_difficutly_level = 4; }
        if ((game_session_time > 180) && (game_session_time < 225)) { game_timer_difficutly_level = 5; }
        if ((game_session_time > 225) && (game_session_time < 270)) { game_timer_difficutly_level = 6; }
        if ((game_session_time > 270)) { game_timer_difficutly_level = 7; }
        */
        //game_timer_difficutly_level = 7;
    }
    //----------------------------------------
    void EnemyNextSpawnTime()
    //----------------------------------------
    {
        enemy_spawn_timer += Time.deltaTime;
        if (enemy_spawn_timer >= enemy_spawn_time_density)
        {
            enemy_spawn_timer = 0;
            EnemyRandomizeSpawnType();
            EnemyRandomizeSpawnPosition();
            EnemySpawnNow();
            //print(spawn_enemy_position + "|" + spawn_enemy_type);
        }
        if (game_difficutly_level == 7)
        {
            enemy_spawn_time_timer += Time.deltaTime;
            if (enemy_spawn_time_timer >= 1)
            {
                enemy_spawn_time_density += 0.02f;
            }
        }
    }
    //----------------------------------------
    void DifficultySetByTime()
    //----------------------------------------
    {
        if (game_difficutly_level == 1)
        {
            my_camera_zoom_max = 40;//45
            my_camera_zoom_increase_value = 0.02f;  // 0.05f;
            box_rotation_speed = 150.0f;
            box_rotation_time_limit = 0.5999997f;
            enemy_speed_min = 4;//4
            enemy_speed_max = 7;//7
            enemy_spawn_time_density = 3.5f;//3.5f
            enemy_speed_increase_value = 0.040f;//0.040f
        }
        if (game_difficutly_level == 2)
        {
            my_camera_zoom_max = 42;//52
            my_camera_zoom_increase_value = 0.02f;
            box_rotation_speed = 200.0f;
            box_rotation_time_limit = 0.4496084f;
            enemy_speed_min = 7;//7
            enemy_speed_max = 9;//9
            enemy_spawn_time_density = 3.0f;//3.0f
            enemy_speed_increase_value = 0.040f;
        }
        if (game_difficutly_level == 3)
        {
            my_camera_zoom_max = 44;//59
            my_camera_zoom_increase_value = 0.02f;
            box_rotation_speed = 250.0f;
            box_rotation_time_limit = 0.3599000f;
            enemy_speed_min = 9;//9
            enemy_speed_max = 11;//11
            enemy_spawn_time_density = 2.5f;//2.5f
            enemy_speed_increase_value = 0.040f;
        }
        if (game_difficutly_level == 4)
        {
            my_camera_zoom_max = 46;//66
            my_camera_zoom_increase_value = 0.02f;
            box_rotation_speed = 300.0f;
            box_rotation_time_limit = 0.29000005f;
            enemy_speed_min = 11;//11
            enemy_speed_max = 13;//13
            enemy_spawn_time_density = 2.0f;//2.2f
            enemy_speed_increase_value = 0.040f;
        }
        if (game_difficutly_level == 5)
        {
            my_camera_zoom_max = 48;//74
            my_camera_zoom_increase_value = 0.02f;
            box_rotation_speed = 450.0f;
            box_rotation_time_limit = 0.19635f;
            enemy_speed_min = 13;//13
            enemy_speed_max = 15;//15
            enemy_spawn_time_density = 1.6f;//2.0f
            enemy_speed_increase_value = 0.040f;
        }
        if (game_difficutly_level == 6)
        {
            my_camera_zoom_max = 50;//81
            my_camera_zoom_increase_value = 0.02f;
            box_rotation_speed = 500.0f;
            box_rotation_time_limit = 0.1766999f;
            enemy_speed_min = 15;//15
            enemy_speed_max = 17;//17
            enemy_spawn_time_density = 1.4f;//1.8f
            enemy_speed_increase_value = 0.040f;
        }
        if (game_difficutly_level == 7)
        {
            my_camera_zoom_max = 50;//145
            my_camera_zoom_increase_value = 0.02f;
            box_rotation_speed = 600.0f;
            box_rotation_time_limit = 0.145000025f;
            enemy_speed_min = 17;//10
            enemy_speed_max = 17;//10 //19
            enemy_spawn_time_density = 1.1f;//1.1f 
            enemy_speed_increase_value = 0.040f;
        }

        //  Apply:
        if (_BoxRot.rotationManipulationIsEnabled)
        {
            _BoxRot.rotationSpeed = box_rotation_speed;
            _BoxRot.rotationTimeLimit = box_rotation_time_limit;
        }
    }
    //----------------------------------------
    void BoxRotationSpeed()
    //----------------------------------------
    {
        //no
    }

    //----------------------------------------
    public void TemporarySpeedIncrease()
    //----------------------------------------
    {

        temporarySpeedIncreaseTime += Time.deltaTime;
        if (temporarySpeedIncreaseTime >= 0.2f)
        {
            temporarySpeedIncreaseTime = 0;
            //print("TemporarySpeedIncrease");


            foreach (Rigidbody clonedObjects in GameObject.FindObjectsOfType<Rigidbody>())
            {
                if (clonedObjects.name.Contains("Clone"))
                {
                    //float tempSpeed = enemy_speed_current * 8;
                    float tempSpeed = 40;   //enemy_speed_current * 7
                    float x = 0, y = 0, z = 0;
                    Vector3 objectVector = new Vector3(x, y, z);
                    objectVector = clonedObjects.velocity;
                    if (objectVector.x > 0.1f)
                    {
                        //print(objectVector);
                        x = tempSpeed;
                        clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                    }
                    if (objectVector.y > 0.1f)
                    {
                        //print(objectVector);
                        y = tempSpeed;
                        clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                    }
                    if (objectVector.z > 0.1f)
                    {
                        //print(objectVector);
                        z = tempSpeed;
                        clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                    }
                    if (objectVector.x < -0.1f)
                    {
                        //print(objectVector);
                        x = -tempSpeed;
                        clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                    }
                    if (objectVector.y < -0.1f)
                    {
                        //print(objectVector);
                        y = -tempSpeed;
                        clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                    }
                    if (objectVector.z < -0.1f)
                    {
                        //print(objectVector);
                        z = -tempSpeed;
                        clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                    }
                }
            }
        }
    }
    //----------------------------------------
    public void TemporarySpeedRestore()
    //----------------------------------------
    {
        //print("TemporarySpeedRestore");

        foreach (Rigidbody clonedObjects in GameObject.FindObjectsOfType<Rigidbody>())
        {
            if (clonedObjects.name.Contains("Clone"))
            {
                float x = 0, y = 0, z = 0;
                Vector3 objectVector = new Vector3(x, y, z);
                objectVector = clonedObjects.velocity;
                if (objectVector.x > 0.1f)
                {
                    x = enemy_speed_current;
                    clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                }
                if (objectVector.y > 0.1f)
                {
                    y = enemy_speed_current;
                    clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                }
                if (objectVector.z > 0.1f)
                {
                    z = enemy_speed_current;
                    clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                }
                if (objectVector.x < -0.1f)
                {
                    x = -enemy_speed_current;
                    clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                }
                if (objectVector.y < -0.1f)
                {
                    y = -enemy_speed_current;
                    clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                }
                if (objectVector.z < -0.1f)
                {
                    z = -enemy_speed_current;
                    clonedObjects.velocity = transform.root.InverseTransformVector(new Vector3(x, y, z));
                }
            }
        }
    }
    //----------------------------------------
    void EnemySpeed()
    //----------------------------------------
    {
        //  Modify the speed so its keept within the range. values declared with the game_timer_difficutly_level:
        if (enemy_speed_current < enemy_speed_min)
        { enemy_speed_current = enemy_speed_min; }
        if (enemy_speed_current > enemy_speed_max)
        { enemy_speed_current = enemy_speed_max; }
    }
    //----------------------------------------
    void EnemySpeedIncrease()
    //----------------------------------------
    {
        if (enemy_speed_current <= enemy_speed_max)
        {
            enemy_speed_increase_timer += Time.deltaTime;
            if (enemy_speed_increase_timer >= 1)
            {
                enemy_speed_increase_timer = 0;
                //  Increase enemy_speed:
                enemy_speed_current += enemy_speed_increase_value;
                enemy_speed = enemy_speed_current;
            }
        }
    }
    //----------------------------------------
    void CameraZooming()
    //----------------------------------------
    {
        if (my_camera.orthographicSize < my_camera_zoom_max)
        {
            my_camera_zoom_timer += Time.deltaTime;

            if (my_camera_zoom_timer >= 0.3f)
            {
                my_camera_zoom_timer = 0;
                //  Keep zooming out:
                my_camera_zoom += my_camera_zoom_increase_value;
                my_camera.orthographicSize = my_camera_zoom;
            }
        }
    }
    void CollectObjectType()
    {
        //no
    }
    //----------------------------------------
    void ScoreVolumetricLight()
    //----------------------------------------
    {
        //  PLAY VOLUMETRIC LIGHT ANIMATION (This is here because needs time to work, not just trigger - 1 frame):
        if (volumetric_light)
        {
            volumetric_light_timer += Time.deltaTime;

            if (volumetric_light_timer >= 0.001f)
            {
                volumetric_light_timer = 0;
                VolumetricLight_EnableRenderer();
                // Stop Volumetric Light:
                if (volumetric_light_offsetY <= -1.03f)//1.54f
                { VolumetricLight_DisableSpecificType(); }
            }
        }
    }
    //----------------------------------------
    void GUIupdate()
    //----------------------------------------
    {
        time_minutes_seconds_gui.text = game_session_time.ToString("0000.000");

        //  Show gui_custom_text for the shor amount of time:   (gui_custom_text.text = "text";)
        if (gui_custom_text_enabled)
        {
            gui_custom_text_timer += Time.deltaTime;
            if (gui_custom_text_timer > 1.5f)
            {
                gui_custom_text_timer = 0;
                gui_custom_text_enabled = false;
                gui_custom_text.text = "";
            }
        }
    }
    //----------------------------------------
    void ApplyGameSettings()
    //----------------------------------------
    {
        //my_camera.orthographicSize = my_camera_zoom;
        //_BoxRot.rotationSpeed = box_rotation_speed;
        //_BoxRot.rotationTimeLimit = box_rotation_time_limit;
        //enemy_speed = enemy_speed_current;
    }
    //----------------------------------------
    void EnemyRandomizeSpawnType()
    //----------------------------------------
    {
        spawn_enemy_type = UnityEngine.Random.Range(1, 7);
    }
    //----------------------------------------
    void EnemyRandomizeSpawnPosition()
    //----------------------------------------
    {
        spawn_enemy_position = UnityEngine.Random.Range(1, 7);
    }
    //----------------------------------------
    void EnemySpawnNow()
    //----------------------------------------
    {
        //spawn_enemy_type = 7;
        //	Based on random number select from what entity to fire object:
        if (spawn_enemy_position == 1)
        {
            if (spawn_enemy_type == 1) { s_S1B1.enemy_speed = enemy_speed; s_S1B1.EnemySpawn(); enemy_quantity_type_1++; }
            if (spawn_enemy_type == 2) { s_S1B2.enemy_speed = enemy_speed; s_S1B2.EnemySpawn(); enemy_quantity_type_2++; }
            if (spawn_enemy_type == 3) { s_S1B3.enemy_speed = enemy_speed; s_S1B3.EnemySpawn(); enemy_quantity_type_3++; }
            if (spawn_enemy_type == 4) { s_S1B4.enemy_speed = enemy_speed; s_S1B4.EnemySpawn(); enemy_quantity_type_4++; }
            if (spawn_enemy_type == 5) { s_S1B5.enemy_speed = enemy_speed; s_S1B5.EnemySpawn(); enemy_quantity_type_5++; }
            if (spawn_enemy_type == 6) { s_S1B6.enemy_speed = enemy_speed; s_S1B6.EnemySpawn(); enemy_quantity_type_6++; }
            //if (spawn_enemy_type == 7) { s_S1B7.enemy_speed = enemy_speed; s_S1B7.EnemySpawn(); enemy_quantity_type_7++; }
        }
        if (spawn_enemy_position == 2)
        {
            if (spawn_enemy_type == 1) { s_S2B1.enemy_speed = enemy_speed; s_S2B1.EnemySpawn(); enemy_quantity_type_1++; }
            if (spawn_enemy_type == 2) { s_S2B2.enemy_speed = enemy_speed; s_S2B2.EnemySpawn(); enemy_quantity_type_2++; }
            if (spawn_enemy_type == 3) { s_S2B3.enemy_speed = enemy_speed; s_S2B3.EnemySpawn(); enemy_quantity_type_3++; }
            if (spawn_enemy_type == 4) { s_S2B4.enemy_speed = enemy_speed; s_S2B4.EnemySpawn(); enemy_quantity_type_4++; }
            if (spawn_enemy_type == 5) { s_S2B5.enemy_speed = enemy_speed; s_S2B5.EnemySpawn(); enemy_quantity_type_5++; }
            if (spawn_enemy_type == 6) { s_S2B6.enemy_speed = enemy_speed; s_S2B6.EnemySpawn(); enemy_quantity_type_6++; }
            //if (spawn_enemy_type == 7) { s_S2B7.enemy_speed = enemy_speed; s_S2B7.EnemySpawn(); enemy_quantity_type_7++; }
        }
        if (spawn_enemy_position == 3)
        {
            if (spawn_enemy_type == 1) { s_S3B1.enemy_speed = enemy_speed; s_S3B1.EnemySpawn(); enemy_quantity_type_1++; }
            if (spawn_enemy_type == 2) { s_S3B2.enemy_speed = enemy_speed; s_S3B2.EnemySpawn(); enemy_quantity_type_2++; }
            if (spawn_enemy_type == 3) { s_S3B3.enemy_speed = enemy_speed; s_S3B3.EnemySpawn(); enemy_quantity_type_3++; }
            if (spawn_enemy_type == 4) { s_S3B4.enemy_speed = enemy_speed; s_S3B4.EnemySpawn(); enemy_quantity_type_4++; }
            if (spawn_enemy_type == 5) { s_S3B5.enemy_speed = enemy_speed; s_S3B5.EnemySpawn(); enemy_quantity_type_5++; }
            if (spawn_enemy_type == 6) { s_S3B6.enemy_speed = enemy_speed; s_S3B6.EnemySpawn(); enemy_quantity_type_6++; }
            //if (spawn_enemy_type == 7) { s_S3B7.enemy_speed = enemy_speed; s_S3B7.EnemySpawn(); enemy_quantity_type_7++; }
        }
        if (spawn_enemy_position == 4)
        {
            if (spawn_enemy_type == 1) { s_S4B1.enemy_speed = enemy_speed; s_S4B1.EnemySpawn(); enemy_quantity_type_1++; }
            if (spawn_enemy_type == 2) { s_S4B2.enemy_speed = enemy_speed; s_S4B2.EnemySpawn(); enemy_quantity_type_2++; }
            if (spawn_enemy_type == 3) { s_S4B3.enemy_speed = enemy_speed; s_S4B3.EnemySpawn(); enemy_quantity_type_3++; }
            if (spawn_enemy_type == 4) { s_S4B4.enemy_speed = enemy_speed; s_S4B4.EnemySpawn(); enemy_quantity_type_4++; }
            if (spawn_enemy_type == 5) { s_S4B5.enemy_speed = enemy_speed; s_S4B5.EnemySpawn(); enemy_quantity_type_5++; }
            if (spawn_enemy_type == 6) { s_S4B6.enemy_speed = enemy_speed; s_S4B6.EnemySpawn(); enemy_quantity_type_6++; }
            //if (spawn_enemy_type == 7) { s_S4B7.enemy_speed = enemy_speed; s_S4B7.EnemySpawn(); enemy_quantity_type_7++; }
        }
        if (spawn_enemy_position == 5)
        {
            if (spawn_enemy_type == 1) { s_S5B1.enemy_speed = enemy_speed; s_S5B1.EnemySpawn(); enemy_quantity_type_1++; }
            if (spawn_enemy_type == 2) { s_S5B2.enemy_speed = enemy_speed; s_S5B2.EnemySpawn(); enemy_quantity_type_2++; }
            if (spawn_enemy_type == 3) { s_S5B3.enemy_speed = enemy_speed; s_S5B3.EnemySpawn(); enemy_quantity_type_3++; }
            if (spawn_enemy_type == 4) { s_S5B4.enemy_speed = enemy_speed; s_S5B4.EnemySpawn(); enemy_quantity_type_4++; }
            if (spawn_enemy_type == 5) { s_S5B5.enemy_speed = enemy_speed; s_S5B5.EnemySpawn(); enemy_quantity_type_5++; }
            if (spawn_enemy_type == 6) { s_S5B6.enemy_speed = enemy_speed; s_S5B6.EnemySpawn(); enemy_quantity_type_6++; }
            //if (spawn_enemy_type == 7) { s_S5B7.enemy_speed = enemy_speed; s_S5B7.EnemySpawn(); enemy_quantity_type_7++; }
        }
        if (spawn_enemy_position == 6)
        {
            if (spawn_enemy_type == 1) { s_S6B1.enemy_speed = enemy_speed; s_S6B1.EnemySpawn(); enemy_quantity_type_1++; }
            if (spawn_enemy_type == 2) { s_S6B2.enemy_speed = enemy_speed; s_S6B2.EnemySpawn(); enemy_quantity_type_2++; }
            if (spawn_enemy_type == 3) { s_S6B3.enemy_speed = enemy_speed; s_S6B3.EnemySpawn(); enemy_quantity_type_3++; }
            if (spawn_enemy_type == 4) { s_S6B4.enemy_speed = enemy_speed; s_S6B4.EnemySpawn(); enemy_quantity_type_4++; }
            if (spawn_enemy_type == 5) { s_S6B5.enemy_speed = enemy_speed; s_S6B5.EnemySpawn(); enemy_quantity_type_5++; }
            if (spawn_enemy_type == 6) { s_S6B6.enemy_speed = enemy_speed; s_S6B6.EnemySpawn(); enemy_quantity_type_6++; }
            //if (spawn_enemy_type == 7) { s_S6B7.enemy_speed = enemy_speed; s_S6B7.EnemySpawn(); enemy_quantity_type_7++; }
        }

        //print(spawn_enemy_position + "|" + spawn_enemy_type);
        enemy_quantity_session_total++;
    }

    // ---------- SCORE  ----------//   (with exception)
    //----------------------------------------
    void OnCollisionEnter(Collision enemy_collected)
    //----------------------------------------
    {
        //  Identify by tag:
        enemy_collected_tag = enemy_collected.gameObject.tag;
        //  Delete/Destroy Enemy:
        Destroy(enemy_collected.gameObject);
        //  Add Score points:
        ScoreADDpoint();
        //  Play Sound:
        PlaySound(0);
        //  Update GUI:
        //ScoreUpdateGUI();     // It is automaticly updated with score add.
        //  Make effect for collecting enemy:       
        VolumetricLight_EnableSpecificType();
    }
    void PlaySound(int clip)
    {   // To play the sound use: "PlaySound(0);"
        AudioSource audio = GetComponent<AudioSource>();
        audio.clip = audioClip[clip];
        audio.Play();
    }
    void GetScoreAndStressValue()
    {
        //  Default Score value: (may change over time ??? TODO) No
        score_collected_enemy_value = 1;
        //  Make stress Decrease value:
        stress_level_increase_decrease_value = -0.15f;
        //  Decrease stress level:       
        stress_level_value += stress_level_increase_decrease_value;
        if (stress_level_value < 0)
        { stress_level_value = 0; }
        //  Apply:
        stress_level_bar.fillAmount = stress_level_value;
    }
    //----------------------------------------
    void ScoreADDpoint()
    //----------------------------------------
    {
        //  Get Score value:
        GetScoreAndStressValue();
        //  Add score point:
        score_current += score_collected_enemy_value;
        //  Check if High score:    (this is problem because of ovrwriting the data of the game several times per session. 
        //CheckHighScore();     // Solution might be to clone scores and compare to them, and then when the game is over store to the original ones).
        //  Add to score_strike:
        ++score_strike;
        //Strike Bonus:
        ++score_strike_bonus_temp;
        if (score_strike_bonus_temp >= 10)                       // Bonus every xx strike poins
        {
            score_strike_bonus_temp = 0;
            ++score_strike_bonus_point;
        }

        ScoreUpdateGUI();
    }
    //----------------------------------------
    void CheckHighScore()
    //----------------------------------------
    {
        //  Retrive scores:
        score_highest_n1 = PlayerPrefs.GetInt("p1_score_highest_n1");
        score_highest_n2 = PlayerPrefs.GetInt("p1_score_highest_n2");
        score_highest_n3 = PlayerPrefs.GetInt("p1_score_highest_n3");

        //  Sort scores:
        int[] score_array = new int[] { score_current, score_highest_n1, score_highest_n2, score_highest_n3 };
        //foreach (int sort in score_array.OrderBy(sorted => sorted)) ;
        Array.Sort(score_array);
        Array.Reverse(score_array);

        //  Write score:
        PlayerPrefs.SetInt("p1_score_highest_n1", score_array[0]);
        PlayerPrefs.SetInt("p1_score_highest_n2", score_array[1]);
        PlayerPrefs.SetInt("p1_score_highest_n3", score_array[2]);
    }
    /*
    static void SortHighScore(string[] args)
    {
        int[] one = { 5, 78, 68, 987, 5, 3, 6, 4 };
        Array.Sort(one);

        for (int i = 0; i < one.Length; i++)
        {
            print(one[i]);
        }
    }*/
    //----------------------------------------
    public void EnemyCollisionWithBox(string _collidedObjetType)    // Error in gameplay: (with exception)
    //----------------------------------------
    {
        //  CALLED FROM EVERY BOX COLLIDER: (CollisionOfBodyAndBox.cs)
        //print("Collided With: " + _collidedObjetType);
        // Check if this is object we actually must hit with box:
        if (enemy_collected_tag == "BodyType7N")
        {
            //print("2");
            PlaySound(2);
            return;
        }
        // Play Sound: (Error)
        PlaySound(1);
        //  Increase stress level:
        stress_level_increase_decrease_value = 0.34f;   // 1.15f; //0.34f //0.50f
        stress_level_value += stress_level_increase_decrease_value;
        //  Apply:
        stress_level_bar.fillAmount = stress_level_value;
        //  Reset score_strike:
        score_strike = 0;
        score_strike_save1 = 0;
        score_strike_bonus_temp = 0;
    }
    //----------------------------------------
    void ScoreUpdateGUI()
    //----------------------------------------
    {
        score_gui.text = "Score: " + (000000 + score_current).ToString();

        score_strike_bonus.text = score_strike_bonus_point.ToString();

        if (score_strike > 1)
        {
            gui_custom_text.text = "Score strike  " + score_strike;
            gui_custom_text_enabled = true;
        }
    }
    //----------------------------------------
    void DestroyAllClonedEnemies()
    //----------------------------------------
    {
        foreach (GameObject clonedObjects in GameObject.FindObjectsOfType<GameObject>())
        {
            if (clonedObjects.name.Contains("Clone"))
            {
                //print("Clone Destroied: " + clonedObjects.name);
                Destroy(clonedObjects.gameObject);
            }
        }
        Resources.UnloadUnusedAssets();
    }
    //----------------------------------------
    void BonusDoubleTap()
    //----------------------------------------
    {
        if (bonusDoubleTapEnabled)
        {
            bonusDoubleTapCircle.gameObject.SetActive(true);
            doubleTapTimer += Time.deltaTime;
            if (doubleTapTimer >= 0.03f)                                                                        // Increase the circle
            {
                doubleTapTimer = 0;
                bonusDoubleTapCircle.gameObject.transform.localScale += new Vector3(80, 80, 0);
                if (bonusDoubleTapCircle.gameObject.transform.localScale.x >= 3500)                              // Reset scale
                {
                    bonusDoubleTapEnabled = false;
                    bonusDoubleTapCircle.gameObject.transform.localScale = new Vector3(1, 1, 0);
                    //bonusDoubleTapCircle.gameObject.SetActive(false);
                }
            }
        }
        //bonusDoubleTapEnabled = false;
    }
    //----------------------------------------
    public void BonusDestroyAllClonedEnemies()
    //----------------------------------------
    {
        if (score_strike_bonus_point >= 1)
        {
            PlaySound(2);
            bonusDoubleTapEnabled = true;
            --score_strike_bonus_point;
            foreach (GameObject clonedObjects in GameObject.FindObjectsOfType<GameObject>())
            {
                if (clonedObjects.name.Contains("Clone"))
                {
                    //print("Clone Destroied: " + clonedObjects.name);
                    Destroy(clonedObjects.gameObject);
                    ScoreADDpoint();
                }
            }
        }
        Resources.UnloadUnusedAssets();
    }
    //----------------------------------------
    void VolumetricLight_EnableRenderer()
    //----------------------------------------
    {
        volumetric_light_offsetY = volumetric_light_offsetY - 0.018f;  //0.03f
        if (enemy_collected_tag == "BodyType1") { VolumetricLight_rendererType1.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_collected_tag == "BodyType2") { VolumetricLight_rendererType2.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_collected_tag == "BodyType3") { VolumetricLight_rendererType3.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_collected_tag == "BodyType4") { VolumetricLight_rendererType4.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_collected_tag == "BodyType5") { VolumetricLight_rendererType5.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
        if (enemy_collected_tag == "BodyType6") { VolumetricLight_rendererType6.material.mainTextureOffset = new Vector2(0, volumetric_light_offsetY); }
    }
    //----------------------------------------
    void VolumetricLight_EnableSpecificType()
    //----------------------------------------
    {
        volumetric_light_offsetY = -0.54f;
        volumetric_light = true;
        if (enemy_collected_tag == "BodyType1") { VolumetricLight_objectType1.gameObject.SetActive(true); }
        if (enemy_collected_tag == "BodyType2") { VolumetricLight_objectType2.gameObject.SetActive(true); }
        if (enemy_collected_tag == "BodyType3") { VolumetricLight_objectType3.gameObject.SetActive(true); }
        if (enemy_collected_tag == "BodyType4") { VolumetricLight_objectType4.gameObject.SetActive(true); }
        if (enemy_collected_tag == "BodyType5") { VolumetricLight_objectType5.gameObject.SetActive(true); }
        if (enemy_collected_tag == "BodyType6") { VolumetricLight_objectType6.gameObject.SetActive(true); }
    }
    //----------------------------------------
    void VolumetricLight_DisableSpecificType()
    //----------------------------------------
    {
        volumetric_light = false;
        //  Just Disable all:
        VolumetricLight_objectType1.gameObject.SetActive(false);
        VolumetricLight_objectType2.gameObject.SetActive(false);
        VolumetricLight_objectType3.gameObject.SetActive(false);
        VolumetricLight_objectType4.gameObject.SetActive(false);
        VolumetricLight_objectType5.gameObject.SetActive(false);
        VolumetricLight_objectType6.gameObject.SetActive(false);
        /*
        if (enemy_collected_tag == "BodyType1") { VolumetricLight_objectType1.gameObject.SetActive(false); }
        if (enemy_collected_tag == "BodyType2") { VolumetricLight_objectType2.gameObject.SetActive(false); }
        if (enemy_collected_tag == "BodyType3") { VolumetricLight_objectType3.gameObject.SetActive(false); }
        if (enemy_collected_tag == "BodyType4") { VolumetricLight_objectType4.gameObject.SetActive(false); }
        if (enemy_collected_tag == "BodyType5") { VolumetricLight_objectType5.gameObject.SetActive(false); }
        if (enemy_collected_tag == "BodyType6") { VolumetricLight_objectType6.gameObject.SetActive(false); }
        */
    }
    //----------------------------------------
    void EnemySpawnPositionCheck()
    //----------------------------------------
    {
        //  Check if needed to move spawners far out:
        enemy_spawn_position_check_timer += Time.deltaTime;
        if (enemy_spawn_position_check_timer >= 3)
        {
            //print("Camera: " + my_camera_zoom);
            my_camera_zoom = my_camera.orthographicSize;
            enemy_spawn_position_check_timer = 0;
            if (my_camera_zoom >= 35)
            {
                S1z = S3x = S5y = -55;
                S2z = S4x = S6y = +55;
            }
            if (my_camera_zoom >= 36)
            {
                S1z = S3x = S5y = -62;
                S2z = S4x = S6y = +62;
            }
            if (my_camera_zoom >= 37)
            {
                S1z = S3x = S5y = -66;
                S2z = S4x = S6y = +66;
            }
            if (my_camera_zoom >= 39.5)
            {
                S1z = S3x = S5y = -70;
                S2z = S4x = S6y = +70;
            }
            if (my_camera_zoom >= 40.5)
            {
                S1z = S3x = S5y = -73;
                S2z = S4x = S6y = +73;
            }
            if (my_camera_zoom >= 44)
            {
                S1z = S3x = S5y = -76;
                S2z = S4x = S6y = +76;
            }
            if (my_camera_zoom >= 48)
            {
                S1z = S3x = S5y = -96;
                S2z = S4x = S6y = +96;
            }
            if (my_camera_zoom >= 58)//
            {
                S1z = S3x = S5y = -106;
                S2z = S4x = S6y = +106;
            }
            if (my_camera_zoom >= 68)
            {
                S1z = S3x = S5y = -120;
                S2z = S4x = S6y = +120;
            }
            if (my_camera_zoom >= 78)
            {
                S1z = S3x = S5y = -135;
                S2z = S4x = S6y = +135;
            }
            if (my_camera_zoom >= 88)
            {
                S1z = S3x = S5y = -150;
                S2z = S4x = S6y = +150;
            }
            if (my_camera_zoom >= 100)
            {
                S1z = S3x = S5y = -172;
                S2z = S4x = S6y = +172;
            }
            if (my_camera_zoom >= 115)
            {
                S1z = S3x = S5y = -198;
                S2z = S4x = S6y = +198;
            }
            if (my_camera_zoom >= 130)
            {
                S1z = S3x = S5y = -222;
                S2z = S4x = S6y = +222;
            }
            if (my_camera_zoom >= 150)
            {
                S1z = S3x = S5y = -222;
                S2z = S4x = S6y = +222;
            }

            //  Apply:
            S1.transform.position = new Vector3(0, 0, S1z);
            S2.transform.position = new Vector3(0, 0, S2z);
            S3.transform.position = new Vector3(S3x, 0, 0);
            S4.transform.position = new Vector3(S4x, 0, 0);
            S5.transform.position = new Vector3(0, S5y, 0);
            S6.transform.position = new Vector3(0, S6y, 0);
        }
    }
    //----------------------------------------
    void GameOverCheck()
    //----------------------------------------
    {
        if (stress_level_value >= 1)
        {
            stress_level_value = 1;
            if (!immortal)
            {
                GameOverNow();
                // Show ads:

            }
        }
    }
    //----------------------------------------
    //  Only called by the checker GameOverCheck() or the "admin". Happens only 1x on gameover event:
    void GameOverNow()
    //----------------------------------------
    {
        //gui_custom_text.text = "Game Over";
        //_ChartBoost_ShowAds.ShowInterstitialAd();
        //_ChartboostExample.ShowInterstitial();
        CheckHighScore();
        DestroyAllClonedEnemies();
        main_menu_delay_countdown = true;
        GAME_start_from_beginning = false;
        _Fade_In_Out.fade_to_black = true;
        game_is_on = false;
        _Keyboard.input_enabled = false;
        _SwipeTouch.input_enabled = false;
        PauseButton_gui.gameObject.SetActive(false);
        //game_over_time = game_session_time;
        //MainMenu_PopUp();
        //score_highest_gui.text = "High Score: " + (PlayerPrefs.GetInt("p1_score_highest").ToString());
    }
    void AdditionalOptions()
    {

    }
    //----------------------------------------
    void MainMenuDelay()    // Delay the ability to press "restart game" before the "fadeOut" is complete:
    //----------------------------------------
    {
        // Delayed Main Menu:
        if (main_menu_delay_countdown == true)
        {
            main_menu_delay_timer += Time.deltaTime;
            if (main_menu_delay_timer >= 1.0f)
            {
                main_menu_delay_countdown = false;
                main_menu_delay_timer = 0;
                //
                ScreenMainMenu_gui.SetActive(true);
                //_ChartBoost_ShowAds.ShowInterstitialAd();
            }
        }
    }
    //----------------------------------------
    void MainMenu_PopUp()
    //----------------------------------------
    {
        //ScreenMainMenu_gui.SetActive(true);
        //score_highest_gui.text = "High Score: " +(PlayerPrefs.GetInt("p1_score_highest").ToString());
    }


    /*
    void test()
    {
        Debug.Log("test:");
        //volumetric_light = false;
        for (int i = 1; i <= 8; i++)
        {
            //Debug.Log(i);
            //volumetric_light_offsetY = i;
            VolumetricLight_EnableRenderer();
        }
        VolumetricLight_DisableSpecificType();
    }
    */
}
