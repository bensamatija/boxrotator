﻿using UnityEngine;
using System.Collections;
//using System;

public class BackgroundColorChanger : MonoBehaviour {

    //private Material backgroundMaterial;
    public GameObject backgroundMesh;
    public Renderer rend;
    public Color myColorTop, myColorBottom;

    public float r_top, g_top, b_top, a;
    public float r_bottom, g_bottom, b_bottom;
    //public float dr, dg, db;
    public float d_random;
    //public int plus_minus = 0;
    public bool increase_r_top = false;
    public bool increase_g_top = false;
    public bool increase_b_top = false;
    public bool increase_r_bottom = false;
    public bool increase_g_bottom = false;
    public bool increase_b_bottom = false;

    private float amplitude = 0.0002f;   //0.015f;
    private float period = 80.5f;    //  0.01f //10.5f

    private float r_x;
    private float r_y;



    public int randomIncrease;

   //private float d_min = 0.0001f;   //0.001f;
    //private float d_max = 0.01f;    //0.03f;

    private float t_colorUpdateTime;


    // Use this for initialization
    void Start ()
    {
        Randomize();
    }

    public void Randomize()
    {
        r_top = Random.Range(0.01f, 1.00f);
        g_top = Random.Range(0.01f, 1.00f);
        b_top = Random.Range(0.01f, 1.00f);
        a = 1;
        r_bottom = Random.Range(0.01f, 1.00f);
        g_bottom = Random.Range(0.01f, 1.00f);
        b_bottom = Random.Range(0.01f, 1.00f);

        // Randomize Increase or decrease:
        randomIncrease = Random.Range(0, 2);
        if (randomIncrease == 1) { increase_r_bottom = true; }
        randomIncrease = Random.Range(0, 2);
        if (randomIncrease == 1) { increase_b_bottom = true; }
        randomIncrease = Random.Range(0, 2);
        if (randomIncrease == 1) { increase_g_bottom = true; }

        randomIncrease = Random.Range(0, 2);
        if (randomIncrease == 1) { increase_r_top = true; }
        randomIncrease = Random.Range(0, 2);
        if (randomIncrease == 1) { increase_b_top = true; }
        randomIncrease = Random.Range(0, 2);
        if (randomIncrease == 1) { increase_g_top = true; }
    }
    
    // Update is called once per frame
    void Update () {

        // Sinus function:
        r_x = Time.timeSinceLevelLoad / period;
        r_y = amplitude * Mathf.Sin(r_x);
        //r_y2 = amplitude * Mathf.Cos(r_x);
        //print(r_y);
        d_random = Mathf.Abs(r_y);




        t_colorUpdateTime += Time.deltaTime;
        if (t_colorUpdateTime >= 0.02f)  //0.1f
        {
            t_colorUpdateTime = 0;

            //backgroundMaterial = backgroundMesh.gameObject.GetComponent<Renderer>().sharedMaterials[0];// .materials[0];
            rend = backgroundMesh.gameObject.GetComponent<Renderer>();

            //--------------------------------------------------------------------------
            // BOTTOM COLOR:

            // Increase or decrease value limit value:
            if (r_bottom >= 0.8f)
            { increase_r_bottom = false;}
            if(g_bottom >= 0.8f)
            { increase_g_bottom = false; }
            if(b_bottom >= 0.8f)
            { increase_b_bottom = false; }

            if (r_bottom <= 0.25f)      //0.1f
            { increase_r_bottom = true; }
            if (g_bottom <= 0.25f)
            { increase_g_bottom = true; }
            if (b_bottom <= 0.25f)
            { increase_b_bottom = true; }

            // Increasing:
            //d_random = Mathf.Abs(r_y);      //d_random = Mathf.Sqrt(Mathf.Abs(r_y));
            if (increase_r_bottom)
            { r_bottom = r_bottom + d_random;}
            //d_random = Mathf.Abs(r_y);
            if (increase_g_bottom)
            { g_bottom = g_bottom + d_random; }
            //d_random = Mathf.Abs(r_y);
            if (increase_b_bottom)
            { b_bottom = b_bottom + d_random; }

            // Decreasing:
            //d_random = Mathf.Abs(r_y2);
            if (!increase_r_bottom)
            { r_bottom = r_bottom - d_random; }
            //d_random = Mathf.Abs(r_y2);
            if (!increase_g_bottom)
            { g_bottom = g_bottom - d_random; }
            //d_random = Mathf.Abs(r_y2);
            if (!increase_b_bottom)
            { b_bottom = b_bottom - d_random; }

            // Apply:
            myColorBottom = new Color(r_bottom, g_bottom, b_bottom, a);
            rend.material.SetColor("_BottomColor", myColorBottom);

            //--------------------------------------------------------------------------
            // TOP COLOR:

            // Increase or_top decrease value limit value:
            if (r_top >= 0.8f)
            { increase_r_top = false; }
            if (g_top >= 0.8f)
            { increase_g_top = false; }
            if (b_top >= 0.8f)
            { increase_b_top = false; }

            if (r_top <= 0.25f)
            { increase_r_top = true; }
            if (g_top <= 0.25f)
            { increase_g_top = true; }
            if (b_top <= 0.25f)
            { increase_b_top = true; }

            // Increasing:
            //d_random = Mathf.Abs(r_y);
            if (increase_r_top)
            { r_top = r_top + d_random; }
            //d_random = Mathf.Abs(r_y);
            if (increase_g_top)
            { g_top = g_top + d_random; }
            //d_random = Mathf.Abs(r_y);
            if (increase_b_top)
            { b_top = b_top + d_random; }

            // Decreasing:
            //d_random = Mathf.Abs(r_y2);
            if (!increase_r_top)
            { r_top = r_top - d_random; }
            //d_random = Mathf.Abs(r_y2);
            if (!increase_g_top)
            { g_top = g_top - d_random; }
            //d_random = Mathf.Abs(r_y2);
            if (!increase_b_top)
            { b_top = b_top - d_random; }

            // Apply:
            myColorTop = new Color(r_top, g_top, b_top, a);
            rend.material.SetColor("_TopColor", myColorTop);

        }     
    }
}
