﻿using UnityEngine;
using System.Collections;

public class Keyboard : MonoBehaviour
{
    public BoxRot _BoxRot;
    public ArrayList key_press_array = new ArrayList(8);
    public bool input_enabled = true;
    public bool array_has_value = false;
    public int array_position = 0;
    public int i = 0;

    // Update is called once per frame
    void Update()
    {
        //--------------------------------
        // Check for Keyboard key press:
        //--------------------------------
        if (input_enabled)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                UP();
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                DOWN();
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                LEFT();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                RIGHT();
            }
        }
    }
    //--------------------------------
    // Buffer the value:
    //--------------------------------
    public void UP()
    {
        key_press_array.Add(1);
        array_has_value = true;
    }
    public void DOWN()
    {
        key_press_array.Add(2);
        array_has_value = true;
    }
    public void LEFT()
    {
        key_press_array.Add(3);
        array_has_value = true;
    }
    public void RIGHT()
    {
        key_press_array.Add(4);
        array_has_value = true;
    }

    //--------------------------------
    //  Do the required rotation:
    //--------------------------------
    void FixedUpdate()
    {
        if (array_has_value)
        {
            if (_BoxRot.rotationManipulationIsEnabled)
            {
                if ((int)key_press_array[array_position] == 1)
                {
                    //print("1");
                    _BoxRot.rotationManipulationIsEnabled = false;
                    _BoxRot.rotate_XP90 = true;
                    _BoxRot.Rotate_XP90();
                    key_press_array.RemoveAt(array_position);
                }
            }
            if (_BoxRot.rotationManipulationIsEnabled)
            {
                if ((int)key_press_array[array_position] == 2)
                {
                    //print("2");
                    _BoxRot.rotationManipulationIsEnabled = false;
                    _BoxRot.rotate_XN90 = true;
                    _BoxRot.Rotate_XN90();
                    key_press_array.RemoveAt(array_position);
                }
            }
            if (_BoxRot.rotationManipulationIsEnabled)
            {
                if ((int)key_press_array[array_position] == 3)
                {
                    //print("3");
                    _BoxRot.rotationManipulationIsEnabled = false;
                    _BoxRot.rotate_ZP90 = true;
                    _BoxRot.Rotate_ZP90();
                    key_press_array.RemoveAt(array_position);
                }
            }
            if (_BoxRot.rotationManipulationIsEnabled)
            {
                if ((int)key_press_array[array_position] == 4)
                {
                    //print("4");
                    _BoxRot.rotationManipulationIsEnabled = false;
                    _BoxRot.rotate_ZN90 = true;
                    _BoxRot.Rotate_ZN90();
                    key_press_array.RemoveAt(array_position);
                }
            }
        }

        if ((key_press_array.Count == 0) && array_has_value == true)
        {
            array_has_value = false;
            key_press_array.Capacity = 0;
            //key_press_array.Clear();
        }
    }
}