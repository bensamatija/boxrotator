﻿using UnityEngine;
using System.Collections;

public class Breathe : MonoBehaviour
{

    public float amplitude = 1f;
    public float period = 0.001f;

    void Start()
    {

    }

     void Update()
    {
        float theta = Time.timeSinceLevelLoad / period;
        float distance = amplitude * Mathf.Sin(theta);
        print(distance);
    }
}