﻿using UnityEngine;
using System.Collections;

public class SwipeTouch3 : MonoBehaviour
{

    private Vector3 fp;   //First touch position
    private Vector3 lp;   //Last touch position
    private float dragDistance = 0;  //minimum distance for a swipe to be registered
    public bool input_enabled = true;
    public bool speedIncrease_enabled = false;

    public int tapCounter = 0;
    public float doubleTapTimer = 0;

    public Keyboard _Keyboard;
    public BaseCode _BaseCode;

    void Start()
    {
        dragDistance = Screen.height * 2.0f / 100; //dragDistance is 15% height of the screen   //1f
    }
    //----------------------------------------
    public void EnableDisable_Input()
    //----------------------------------------
    {
        if (input_enabled) { input_enabled = false; return; }
        if (!input_enabled) { input_enabled = true; return; }
    }

    void Update()
    //void FixedUpdate()        // Having this enabled rotates the box for 180 instead of 90 ?? !!
    {
        if (input_enabled)
        {
            if (Input.touchCount == 1) // user is touching the screen with a single touch
            {
                Touch touch = Input.GetTouch(0); // get the touch
                if (touch.phase == TouchPhase.Began) //check for the first touch
                {
                    //print("Tap started");
                    tapCounter++;
                    fp = touch.position;
                    lp = touch.position;
                }
                else if (touch.phase == TouchPhase.Moved) // update the last position based on where they moved
                {
                    lp = touch.position;
                }
                else if (touch.phase == TouchPhase.Ended) //check if the finger is removed from the screen
                {
                    lp = touch.position;  //last touch position. Ommitted if you use list

                    if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
                    {//It's a drag
                        _BaseCode.temporarySpeedIncreaseTime = 0;
                        _BaseCode.TemporarySpeedRestore();
                        //X+
                        if ((lp.x - fp.x) > 0)
                        {
                            //Y+
                            if ((lp.y - fp.y) > 0)
                            {
                                _Keyboard.RIGHT();
                            }
                            //Y-
                            else
                            {
                                _Keyboard.DOWN();
                            }
                        }
                        //X-
                        else
                        {
                            //Y+
                            if ((lp.y - fp.y) > 0)
                            {
                                _Keyboard.UP();
                            }
                            //Y-
                            else
                            {
                                _Keyboard.LEFT();
                            }
                        }
                    }
                    //else if (touch.phase != TouchPhase.Ended)
                    else
                    {
                        //Debug.Log("Tap ended");
                        _BaseCode.temporarySpeedIncreaseTime = 0;
                        _BaseCode.TemporarySpeedRestore();
                    }
                }
                //Holding down the Touch:
                if ((touch.phase == TouchPhase.Stationary))
                {
                    //print("Touching down");
                    _BaseCode.TemporarySpeedIncrease();
                }
            }
        }
        // Check if first from doubletap happenes:
        if(tapCounter > 0)
        {
            doubleTapTimer += Time.deltaTime;
            // Check if second from doubletap happenes:
            if (doubleTapTimer > 0.50f)//0.25f //0.20f
            {
                doubleTapTimer = 0;
                tapCounter = 0;
            }
            if(tapCounter >= 3)
            {
                tapCounter = 0;
                doubleTapTimer = 0;
                print("Enable extra powers");
                _BaseCode.bonusDoubleTapEnabled = true;
                _BaseCode.BonusDestroyAllClonedEnemies();
            }
        }
    }
}
